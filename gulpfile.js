var gulp          = require('gulp');
var notify        = require('gulp-notify');
var source        = require('vinyl-source-stream');
var browserify    = require('browserify');
var babelify      = require('babelify');
var ngAnnotate    = require('browserify-ngannotate');
var browserSync   = require('browser-sync').create();
var rename        = require('gulp-rename');
var templateCache = require('gulp-angular-templatecache');
var uglify        = require('gulp-uglify');
var merge         = require('merge-stream');
var args		  = require('yargs').argv;
var gulpif		  = require('gulp-if');
var nunjucks	  = require('gulp-nunjucks');

var isProduction = args.env === 'prod';

// Where our files are located
var jsFiles   = "src/js/**/*.js";
var viewFiles = "src/js/**/*.html";
var jslibraries = ['./node_modules/jquery/dist/jquery.min.js',
					'./node_modules/bootstrap/dist/js/bootstrap.min.js',
					'./node_modules/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js',
					'./node_modules/moment/min/moment.min.js',
					'./node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
					'./node_modules/underscore/underscore-min.js',
					'./src/js/scripts/jsoneditor.min.js',
					'./src/js/scripts/FileSaver.min.js',
					'./node_modules/jquery-ui-dist/jquery-ui.min.js',
					'./node_modules/clipboard/dist/clipboard.min.js'];
var csslibraries = ['./node_modules/animate.css/animate.min.css',
					'./node_modules/bootstrap/dist/css/bootstrap.min.css',
					'./node_modules/flat-ui/css/flat-ui.css',
					'./node_modules/font-awesome/css/font-awesome.min.css',
					'./node_modules/nvd3/build/nv.d3.min.css',
					'./node_modules/jquery-datetimepicker/build/jquery.datetimepicker.min.css',
					'./src/css/style.css',
					'./node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',
					'./node_modules/jsoneditor/dist/jsoneditor.min.css',
                    './node_modules/angular-loading-bar/build/loading-bar.min.css',
                    './src/css/material-design-switch.css'];

var interceptErrors = function(error) {
  var args = Array.prototype.slice.call(arguments);

  // Send error to notification center with gulp-notify
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);

  // Keep gulp from hanging on this task
  this.emit('end');
};


gulp.task('browserify', ['views'], function() {
  return browserify('./src/js/app.js')
      .transform(babelify, {presets: ["es2015"]})
      .transform(ngAnnotate)
      .bundle()
      .on('error', interceptErrors)
      //Pass desired output filename to vinyl-source-stream
      .pipe(source('main.js'))
      // Start piping stream to tasks!
      .pipe(gulp.dest('./'));
});

gulp.task('html', function() {
  return gulp.src("src/index.html")
      .on('error', interceptErrors)
	  .pipe(nunjucks.compile({
				env: args.env
		}))
      .pipe(gulp.dest('./'));
});

gulp.task('views', function() {
  return gulp.src(viewFiles)
      .pipe(templateCache({
        standalone: true
      }))
      .on('error', interceptErrors)
      .pipe(rename("app.templates.js"))
      .pipe(gulp.dest('./src/js/config/'));
});

gulp.task('copyFiles', function() {
   gulp.src(jslibraries)
   .pipe(gulp.dest('./js/'));
   gulp.src(csslibraries)
   .pipe(gulp.dest('./css/'));
   gulp.src('./src/css/img/jsoneditor-icons.svg')
   .pipe(gulp.dest('./css/img/'));
   gulp.src('./src/images/*.*')
   .pipe(gulp.dest('./images/'));
    gulp.src('./src/fonts/*.*')
   .pipe(gulp.dest('./fonts/'));
});

// This task is used for building production ready
// minified JS/CSS files into the destination/ folder
/*gulp.task('build', ['html', 'browserify'], function() {
  var html = gulp.src("destination/index.html")
                 .pipe(gulp.dest('./destination/'));

  var js = gulp.src("destination/main.js")
               .pipe(gulpif(isProduction, uglify()))
               .pipe(gulp.dest('./destination/'));

  return merge(html,js);
});*/

gulp.task('default', ['html', 'browserify', 'copyFiles'], function() {

});

gulp.task('local', ['html', 'browserify', 'copyFiles'], function() {

  browserSync.init(['./main.js'], {
    server: "./",
    port: 4000,
    notify: false,
    ui: {
      port: 4001
    }
  });

  gulp.watch("src/index.html", ['html']);
  gulp.watch(viewFiles, ['views']);
  gulp.watch(jsFiles, ['browserify']);
});
