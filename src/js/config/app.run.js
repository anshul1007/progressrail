function AppRun($rootScope, $state, User) {
    'ngInject';

    // change page title based on state
    //$rootScope.$on('$stateChangeSuccess', (event, toState) => {
    //  $rootScope.setPageTitle(toState.title);
    //});

    $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
        var skipLogin = !!toState.data && !!toState.data.skipLogin;
        if (skipLogin !== true && !User.current) {
            event.preventDefault();
            User.checkCurrentUser().then(
            () => {
                $state.go(toState, toParams);
            }, 
            () => {
                $state.go('app.login');
            });
        }
        else if (skipLogin !== true) {
            User.checkCurrentUser();
        }
    });

    // Helper method for setting the page's title
    //$rootScope.setPageTitle = (title) => {
    //    $rootScope.pageTitle = '';
    //    if (title) {
    //        $rootScope.pageTitle += title;
    //        $rootScope.pageTitle += ' \u2014 ';
    //    }
    //    $rootScope.pageTitle += AppConstants.appName;
    //};

}

export default AppRun;
