import BaseApi from '../config/app.baseApi';

const AppConstants = {
    api:  BaseApi.origin + '/api/',
    appName: 'Progress Rail',
    routes:{
        /**------------get-------------------**/
        getGeneralOrganizationData: 'organization/summary/generalOrganizationData?id={0}&currentTime={1}', //api#1
        getSummaryData:'organization/summary/summaryOrganizationData?id={0}&currentTime={1}', //api#2
        getThroughputData: 'organization/summary/throughputData?id={0}&startTime={1}&endTime={2}&interval={3}', //api#3
        getSourceCategories : 'source/categories', //api#6
        getCountries: 'organization/countries', //api#7
        getTags : 'source/tags', //api#9
        getDataTypes : 'source/formats', //api#11
        getProcessingUnitsbyType : 'processingUnit/byType?type={0}&active={1}', //api#12
        getOrganizations : 'organization', //api#13
        getOrganizationForDevice : 'device/organizations', //api#14
        getOrganizationForPipelineView: 'pipeline/view/organizations', //api#15
        getOrganizationForPipelineManage: 'pipeline/manage/organizations', //api#16
        getSourceByOrganizationForDevice: 'device/source/byOrganizationId?organizationId={0}', //api#18
        getSourceByOrganizationForPipelineView: 'pipeline/view/source/byOrganizationId?organizationId={0}', //api#19
        getSourceByOrganizationForPipelineManage: 'pipeline/manage/source/byOrganizationId?organizationId={0}', //api#20
        getSourceByOrganizationForOrganizationViewSummary: 'organization/summary/source/byOrganizationId?organizationId={0}', //api#21
        getDataTypesbySourceCategory : 'source/formats/bySourceCategoryId?categoryId={0}', //api#22
        testPipeline : 'pipeline/test?configurationId={0}&s3Url={1}', //api#24
        searchPipelinePaged : 'pipeline/searchPaged?page={0}&size={1}', //api#26
        lastTestResultForPipeline : 'pipeline/lastTestResult?configurationId={0}', //api#27
        getPipelineById : 'pipeline/byId?configurationId={0}', //api#28
        searchDeviceRegistration :'device/searchPaged?page={0}&size={1}', //api#29
        getDeviceRegistrationInformation  :'device?registrationRequestId={0}', //api#31
        findSourceByNameForDevice : 'device/source/byName?name={0}', //api#34
        findSourceByNameForDeviceContaining : 'device/source/byNameContaining?name={0}',
        getRegistrationStatus : 'device/statuses',  //api#35
        getSourceFormatsByCategoryandTags: 'source/formats/byCategoryAndTags?categoryId={0}', //api#36
        getSourceFormatsByNameAndTags: 'source/formats/bySourceFormatNameAndTags?q={0}', //api#36
        getSourceFormatsBySource: 'source/formats/bySource?sourceId={0}', //api#37
        getAllTestResultForPipeline : 'pipeline/allTestResults?configurationId={0}', //api#40
        downloadPipelineTestOutput : 'pipeline/testOutput?s3Url={0}', //api#41
        getDeviceTimeUnits: 'device/timeUnits', //api#42
        getAllRoles: 'role/all', //api#46
        getAllPermissionsList: 'role/permissions', //api#47
        getUsersByRole: 'role/users?roleId={0}', //api#50
        getPipelineStatuses : 'pipeline/approvalStatuses', //api#51
        getOrganizationsAndRoleByUser : 'user/organization/roles?userId={0}', //api#59
        getUsers : 'user/searchPaged?page={0}&size={1}', //api#60
        getSourceFormatTypes: 'source/format/types', //api#66
        getCurrentUser: 'user/current', //api#67
        getCurrentUserOrganization: 'user/current/organizations', //api#68
        getSourceSummary: 'source/summary?sourceId={0}', //api#70
        getBillingSummary : 'organization/summary/usageStatistics?dateFrom={0}&dateTo={1}&interval={2}', //api#71
        getProcessingUnitTypes : 'processingUnit/types', //api#76
        getAllProcessingUnits : 'processingUnit/all', //api#78
        searchProcessingUnits : 'processingUnit/searchPaged?page={0}&size={1}', //api#84
        getAvailableMatrics : 'alerts/rules/supportedMetrics', //api#82.A
        retrieveAllMetricAlertRules  : 'alerts/rules/{0}/metric', //api#82.C
        retrieveSingleMetricAlertRules  : 'alerts/rules/{0}/metric/{1}', //api#82.D
        retrieveAlerts : 'alerts/{0}/metric', //api#83.A
        getOrganizationsSummary : 'organization/summary/all/summaryData', //api#94
        getIngestionsAndThroughputChartsData : 'organization/summary/all/ingestionCharts?startTime={0}&endTime={1}&interval={2}', //api#96
        getDeadletterCount: 'deadletter/count', //api#97.A
        getUnreadErrorCount: 'error/count', //api#97.A
        getDeadletter : 'deadletter?pageNumber={0}&pageSize={1}', //api#97.B
        getDeadletterMeta : 'deadletter/meta', //api#97.B
        getTopTenIngestionsAndThroughput : 'organization/summary/all/topTenIngestions?startTime={0}&endTime={1}', //api#98
        getOrganizationDetails: 'organization/fullData?id={0}', //api#99
        retrieveAllAlert  : 'alerts/{0}/metric?includeSilenced={1}&includeRead={2}', //api#83.A
        getAWSInstancesStatistics : 'home/awsinstances?startTime={0}&endTime={1}&intervalHours={2}', //api#102
        getProcessingTime :  'organization/summary/timeStatistics?startTime={0}&endTime={1}&intervalHours={2}', //api#103
        retrieveAllError  : 'error?read={0}', //api#104
        /**------------post-------------------**/
        addOrganization: 'organization', //api#4
        addDataType: 'source/format', //api#5
        updateDataType: 'source/format/update', //api#5
        addDevice: 'source', //api#8
        addTag : 'source/tag', //api#10
        addPipeline : 'pipeline', //api#17
        addSourceFormatsToSource: 'source/formats/toSource', //api#38
        addRole : 'role', //api#43
        login: 'security/login?username={0}&password={1}', //api#48
        addUser : 'user', //api#53
        addPermissionsToRole : 'role/permissions?roleId={0}', //api#64
        addRolesToOrganizationsAndUser : 'user/roles/toOrganization?userId={0}&organizationId={1}', //api#57
        getRegistrationCredentials : 'device/credentials?clientId={0}&registrationId={1}', //api#72
        getRenewedRegistrationCredentials : 'device/credentials/renew?clientId={0}&registrationId={1}', //api#72
        addProcessingUnit: 'processingUnit', //api#74
        addProcessingUnitType : 'processingUnit/type', //api#75
        addOrganizationAddress : 'organization/address', //api#86
        addOrganizationContact : 'organization/contact', //api#89
        syncOfMetricAlertRule  : 'alerts/rules/{0}/metric/{1}', //api#82.E
        addCountriesToOrganization   : 'organization/countries', //api#100
        /**------------put-------------------**/
        updatePipeline : 'pipeline', //api#23
        updateDeviceRegistrationInformation: 'device', //api#32
        changePipelineStatus  : 'pipeline/changeStatus?configurationId={0}&status={1}', //api#25
        changeDeviceRegistrationStatus:'device/changeStatus?registrationRequestId={0}&registrationStatus={1}', //api#30
        approveDeviceRegistrationInformation: 'device/approve', //api#33
        updateRole : 'role', //api#44
        updateSourceFormatsToSource: 'source/formats/toSource/update', //api#52
        updateUser : 'user', //api#54
        changePassword: 'user/password?userId={0}&password={1}', //api#61
        makeUserActive : 'user/active?userId={0}', //api#62
        deviceRegister : 'device/register', //api#71
        updateProcessingUnit: 'processingUnit', //api#77
        activateProcessingUnits : 'processingUnit/activate?id={0}', //api#79
        makeUserRoleGlobal : "user/role/global?userId={0}", //api#81
        updateOrganization : 'organization', //api#85
        updateOrganizationAddress : 'organization/address', //api#87
        updateOrganizationContact : 'organization/contact', //api#90
        activateOrganizqation : 'organization/activate?id={0}', //api#92
        activateSource : 'source/activate?id={0}', //api#92
        deactivateSource : 'source/deactivate?id={0}', //api#92
        activateDataType : 'source/formats/fromSource/enable?sourceId={0}', //api#39,
        deactivateDataType : 'source/formats/fromSource/disable?sourceId={0}', //api#39,
        metricAlertRuleAddUpdate: 'alerts/rules/{0}/metric?skipsync={1}', //api#82.B
        setMetricAlertRuleSilenced : 'alerts/rules/{0}/metric/{1}/silence', //api#82.G
        markErrorAsRead : 'error/markAsRead?errorId={0}', //api#105
        /**------------delete-------------------**/
        deleteSourceFormatsToSource: 'source/formats/fromSource?sourceId={0}', //api#39
        deleteRole : 'role?roleId={0}', //api#45
        logout: 'security/login', //api#49
        removeRolesFromOrganizationsAndUser:'user/roles/toOrganization?userId={0}&organizationId={1}', //api#58
        makeInactiveActive : 'user/active?userId={0}', //api#63
        deletePermissionsFromRole : 'role/permissions?roleId={0}', //api#65
        deactivateProcessingUnits : 'processingUnit/activate?id={0}', //api#80
        deactivateOrganizqation : 'organization/activate?id={0}', //api#93
        deleteMetricAlertRule  : 'alerts/rules/{0}/metric/{1}', //api#82.F
        setMetricAlertRuleUnsilenced : 'alerts/rules/{0}/metric/{1}/silence', //api#82.H
        deleteAlerts : 'alerts/{0}/metric/{1}', //api#83.B
        deleteUserRoleGlobal: "user/role/global?userId={0}", //api#81
        deleteCountriesToOrganization   : 'organization/countries', //api#101
        markAlertAsRead : 'alerts/{0}/metric/{1}', //api#83.B
        deleteDataType: 'source/format/delete?id={0}', //api#5
    },
    chartOptions: {
        throughputData : {
            chart: {
                type: 'stackedAreaChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 40
                },
                x: function(d){return d[0];},
                y: function(d){return d[1];},
                useVoronoi: false,
                clipEdge: true,
                duration: 100,
                useInteractiveGuideline: true,
                xAxis: {
                    showMaxMin: false,
                    tickFormat: function(d) {
                        return d3.time.format('%x %X')(new Date(d*1000))
                    }
                },
                yAxis: {
                    tickFormat: function(d){
                        return parseInt(d);
                    }
                },
                zoom: {
                    enabled: true,
                    scaleExtent: [1, 10],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: true,
                    unzoomEventType: 'dblclick.zoom'
                }
            }
        }
    },
    paginationOptions: {
        maxSize: 10,
        itemsPerPage: 25,
        totalItems: 0,
        currentPage: 1
    }
};

export default AppConstants;
