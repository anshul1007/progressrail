function HasPermission(User, $state) {
    'ngInject';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.User = User;

            scope.$watch('User.current', function(val) {
                // If user detected
                if (val) {
                    var userPermission = val.permissions;
                    var hasPermission = _.intersection(val.permissions, attrs.hasPermission.split(','));
                    if ((val.permissions.indexOf('ADMIN')) > -1 || (hasPermission && hasPermission.length > 0)) {
                        element.css({ display: 'inherit'});
                    } else {
                        element.css({ display: 'none'});
                    }
                    // no user detected
                } else {
                    element.css({ display: 'none'});
                }
            });

        }
    };
}

export default HasPermission;
