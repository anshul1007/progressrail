﻿let Percentage= {
    bindings: {
        value: '='
    },
    templateUrl: 'components/controls/percentage.html'
};

export default Percentage;