function DateTimePicker($timeout) {
    'ngInject';

    return {
        restrict: 'A',
        scope : {
            dateTime : '=dateTime'
        },
        link: function (scope, element, attrs) {
            $(element).datetimepicker({
                format: 'm/d/Y H:i',
                maxDate: new Date(),
                //maxTime: moment(new Date()).format('HH:mm'),
                onClose: function (dp) {
                    if (moment(scope.dateTime).format('MM/DD/YYYY HH:mm') !== moment(dp).format('MM/DD/YYYY HH:mm')) {
                        scope.dateTime = moment(dp);
                        $timeout(function() {
                            
                        });
                    }
                }
            });

            $(element).val(moment(scope.dateTime).format('MM/DD/YYYY HH:mm'));

            scope.$watch('dateTime', function () {
                $(element).val(moment(scope.dateTime).format('MM/DD/YYYY HH:mm'));
            });

        }
    };
}

export default DateTimePicker;
