import angular from 'angular';

let componentsModule = angular.module('app.components', []);


import ListErrors from './list-errors.component'
componentsModule.component('listErrors', ListErrors);

import ShowAuthed from './show-authed.directive';
componentsModule.directive('showAuthed', ShowAuthed);

import HasPermission from './has-permission.directive';
componentsModule.directive('hasPermission', HasPermission);

import DateTimePicker from './controls/datetimepicker.directive';
componentsModule.directive('dateTimePicker', DateTimePicker);

import PhoneInput from './controls/phoneInput.directive';
componentsModule.directive('phoneInput', PhoneInput);

import Email from './controls/email.directive';
componentsModule.directive('email', Email);

import Percentage from './controls/percentage.component'
componentsModule.component('percentage', Percentage);

import JsonEditor from './controls/jsoneditor.directive';
componentsModule.directive('jsonEditor', JsonEditor);

export default componentsModule;
