class ProgressRail {
    constructor(AppConstants, Common) {
        'ngInject';

        this._AppConstants = AppConstants;
        this._Common  = Common;
    }

    getSummaryData(id) {
        var url =  this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSummaryData, id, (new Date).valueOf());
        return this._Common.getData(url);
    };

    getGeneralOrganizationData(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getGeneralOrganizationData, id, (new Date).valueOf());
        return this._Common.getData(url);
    };

    getThroughputData(id, dateRange) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getThroughputData, id, dateRange.fromDate.valueOf(), dateRange.toDate.valueOf(), dateRange.interval);
        return this._Common.getData(url);
    };

    getCountries() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getCountries);
        return this._Common.getData(url);
    };

    getSourceCategories() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceCategories);
        return this._Common.getData(url);
    };

    getTags() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getTags);
        return this._Common.getData(url);
    };

    getOrganizations() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizations);
        return this._Common.getData(url);
    };

    getOrganizationForDevice() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationForDevice);
        return this._Common.getData(url);
    };

    getOrganizationForPipelineView() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationForPipelineView);
        return this._Common.getData(url);
    };

    getOrganizationForPipelineManage() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationForPipelineManage);
        return this._Common.getData(url);
    };

    getDataTypes() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDataTypes);
        return this._Common.getData(url);
    };

    getSourceByOrganizationForDevice(organizationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceByOrganizationForDevice, organizationId);
        return this._Common.getData(url);
    };

    getSourceByOrganizationForPipelineView(organizationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceByOrganizationForPipelineView, organizationId);
        return this._Common.getData(url);
    };

    getSourceByOrganizationForPipelineManage(organizationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceByOrganizationForPipelineManage, organizationId);
        return this._Common.getData(url);
    };

    getSourceByOrganizationForOrganizationViewSummary(organizationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceByOrganizationForOrganizationViewSummary, organizationId);
        return this._Common.getData(url);
    };

    getDataTypesbySourceCategory(sourceCategoryId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDataTypesbySourceCategory, sourceCategoryId);
        return this._Common.getData(url);
    };

    getProcessingUnitsbyType(type, active) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getProcessingUnitsbyType, type, active);
        return this._Common.getData(url);
    };

    getUsersByRole(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getUsersByRole, id);
        return this._Common.getData(url);
    };

    findSourceByNameForDevice(name) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.findSourceByNameForDevice, name);
        return this._Common.getData(url);
    };

    findSourceByNameForDeviceContaining(name) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.findSourceByNameForDeviceContaining, name);
        return this._Common.getData(url);
    };

    testPipeline(configurationId, s3Url) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.testPipeline, configurationId, s3Url);
        return this._Common.getData(url);
    };

    searchPipelinePaged(page, size, organizationId, sourceId, sourceFormatId, pipelineName, status) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.searchPipelinePaged,
            page, size);

        if(organizationId) {
            url = url + "&organizationId=" + organizationId
        }

        if(sourceId) {
            url = url + "&sourceId=" + sourceId
        }

        if(sourceFormatId) {
            url = url + "&sourceFormatId=" + sourceFormatId
        }

        if(pipelineName) {
            url = url + "&pipelineName=" + pipelineName
        }

        if(status) {
            url = url + "&statuses=" + status
        }

        return this._Common.getData(url);
    };

    lastTestResultForPipeline(configurationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.lastTestResultForPipeline, configurationId);
        return this._Common.getData(url);
    };

    getPipelineById(configurationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getPipelineById, configurationId);
        return this._Common.getData(url);
    };

    getRegistrationStatus() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getRegistrationStatus);
        return this._Common.getData(url);
    };

    searchDeviceRegistration(page, size, organizationId, sourceId, status) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.searchDeviceRegistration, page, size);

        if(organizationId) {
            url = url + "&organizationId=" + organizationId
        }

        if(sourceId) {
            url = url + "&sourceId=" + sourceId
        }

        _.map(status, function(val) {
            url = url + "&registrationStatus=" + val;
        });
        return this._Common.getData(url);
    };

    getDeviceRegistrationInformation(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeviceRegistrationInformation, id);
        return this._Common.getData(url);
    };

    getSourceFormatsByCategoryandTags(id, tags) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceFormatsByCategoryandTags, id);
        _.map(tags, function(val) {
            url = url + "&tagId=" + val;
        });
        return this._Common.getData(url);
    };

    lookUpDataTypesAndTags(categoryId, searchTerm) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceFormatsByNameAndTags, searchTerm);
        url = url + "&categoryId=" + categoryId;
        return this._Common.getData(url);
    };

    getSourceFormatsBySource(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceFormatsBySource, id);
        return this._Common.getData(url);
    };

    getDeviceTimeUnits() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeviceTimeUnits);
        return this._Common.getData(url);
    };

    getAllTestResultForPipeline(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAllTestResultForPipeline, id);
        return this._Common.getData(url);
    }

    getAllPermissionsList() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAllPermissionsList);
        return this._Common.getData(url);
    }

    getAllRoles() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAllRoles);
        return this._Common.getData(url);
    }

    getPipelineStatuses(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getPipelineStatuses, id);
        return this._Common.getData(url);
    }

    downloadPipelineTestOutput(s3Url) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.downloadPipelineTestOutput, s3Url);
        return this._Common.getData(url);
    }

    getUsers(page, size) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getUsers,
            page, size);

        return this._Common.getData(url);
    };

    getOrganizationsAndRoleByUser(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationsAndRoleByUser, id);
        return this._Common.getData(url);
    }

    getOrganizationsByUser(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationsByUser, id);
        return this._Common.getData(url);
    }

    getSourceFormatTypes() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceFormatTypes);
        return this._Common.getData(url);
    }

    getCurrentUser() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getCurrentUser);
        return this._Common.getData(url);
    }

    getCurrentUserOrganization() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getCurrentUserOrganization);
        return this._Common.getData(url);
    }

    getSourceSummary(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getSourceSummary, id);
        return this._Common.getData(url);
    }

    getBillingSummary(from, to, interval) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getBillingSummary, from, to, interval);
        return this._Common.getData(url);
    }

    getAllProcessingUnits() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAllProcessingUnits);
        return this._Common.getData(url);
    }

    getProcessingUnitTypes() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getProcessingUnitTypes);
        return this._Common.getData(url);
    }

    searchProcessingUnits(page, size, type, className, status, name, description) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.searchProcessingUnits,
            page, size);

        if(type) {
            url = url + "&type=" + type
        }

        if(className) {
            url = url + "&className=" + className
        }

        if(status) {
            url = url + "&status=" + status
        }

        if(name) {
            url = url + "&name=" + name
        }

        if(description) {
            url = url + "&description=" + description
        }

        return this._Common.getData(url);
    };

    getAvailableMatrics() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAvailableMatrics);
        return this._Common.getData(url);
    }

    retrieveAllMetricAlertRules(scope) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.retrieveAllMetricAlertRules, scope);
        return this._Common.getData(url);
    }

    retrieveSingleMetricAlertRules(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.retrieveSingleMetricAlertRules, scope, id);
        return this._Common.getData(url);
    }

    retrieveAlerts(scope) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.retrieveAlerts, scope);
        return this._Common.getData(url);
    }

    getRegistrationCredentials(clientId, registrationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getRegistrationCredentials, clientId, registrationId);
        return this._Common.getData(url);
    };

    getRenewedRegistrationCredentials(clientId, registrationId) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getRenewedRegistrationCredentials, clientId, registrationId);
        return this._Common.getData(url);
    };

    getOrganizationsSummary() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationsSummary);
        return this._Common.getData(url);
    }

    getUnreadErrorCount() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getUnreadErrorCount);
        return this._Common.getData(url);
    }

    getDeadletterCount() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeadletterCount);
        return this._Common.getData(url);
    }

    getDeadletter(pagenumber, pagesize) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeadletter, pagenumber, pagesize);
        return this._Common.getData(url);
    }

    getDeadletterMeta() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeadletterMeta);
        return this._Common.getData(url);
    }

    getDeadletterMetaWithFilter(pagenumber, pagesize, filterObj) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getDeadletter, pagenumber, pagesize);

        if(filterObj) {
            var kvPairs = "";
            for(var key in filterObj) {
                if(kvPairs && kvPairs.trim() == "") {
                    kvPairs = key + "=" + filterObj[key];
                } else {
                    kvPairs = kvPairs + "&" + key + "=" + filterObj[key];
                }
            }
            url = url + kvPairs;
        }

        return this._Common.getData(url);
    }

    getIngestionsAndThroughputChartsData(startTime, endTime, interval) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getIngestionsAndThroughputChartsData, parseInt(startTime.valueOf()/1000), parseInt(endTime.valueOf()/1000), interval);
        return this._Common.getData(url);
    }

    getProcessingTime(startTime, endTime, interval) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getProcessingTime, parseInt(startTime.valueOf()/1000), parseInt(endTime.valueOf()/1000), interval);
        return this._Common.getData(url);
    }

    getAWSInstancesStatistics(startTime, endTime, interval) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getAWSInstancesStatistics, parseInt(startTime.valueOf()/1000), parseInt(endTime.valueOf()/1000), interval);
        return this._Common.getData(url);
    }

    getTopTenIngestionsAndThroughput(startTime, endTime) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getTopTenIngestionsAndThroughput, parseInt(startTime.valueOf()/1000), parseInt(endTime.valueOf()/1000));
        return this._Common.getData(url);
    }

    getOrganizationDetails(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.getOrganizationDetails, id);
        return this._Common.getData(url);
    };

    retrieveAllAlert(scope, includeSilenced, includeRead) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.retrieveAllAlert, scope, includeSilenced, includeRead);
        return this._Common.getData(url);
    };

    retrieveAllError(includeRead) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.retrieveAllError, includeRead);
        return this._Common.getData(url);
    };

    login(user, password) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.login, user, password);
        return this._Common.postData(url);
    }

    addOrganization(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addOrganization);
        return this._Common.postData(url, data);
    };

    addDevice(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addDevice);
        return this._Common.postData(url, data);
    };

    addDataType(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addDataType);
        return this._Common.postData(url, data);
    };

    updateDataType(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateDataType);
        return this._Common.postData(url, data);
    };

    deleteDataType(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteDataType, data);
        return this._Common.deleteData(url);
    };

    addTag(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addTag);
        return this._Common.postData(url, data);
    };

    addPipeline(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addPipeline);
        return this._Common.postData(url, data);
    };

    addSourceFormatsToSource(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addSourceFormatsToSource);
        return this._Common.postData(url, data);
    };

    updateSourceFormatsToSource(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateSourceFormatsToSource);
        return this._Common.putData(url, data);
    };

    addRole(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addRole);
        return this._Common.postData(url, data);
    };

    addUser(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addUser);
        return this._Common.postData(url, data);
    };

    addRolesToOrganizationsAndUser(userId, organizationId, roles) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addRolesToOrganizationsAndUser, userId, organizationId);
        _.map(roles, function(val) {
            url = url + "&roleId=" + val;
        });
        return this._Common.postData(url);
    }

    addPermissionsToRole(id, permissions) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addPermissionsToRole, id);
        _.map(permissions, function(val) {
            url = url + "&permission=" + val;
        });
        return this._Common.postData(url);
    }

    addProcessingUnit(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addProcessingUnit);
        return this._Common.postData(url, data);
    };

    addProcessingUnitType(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addProcessingUnitType);
        return this._Common.postData(url, data);
    };

    addOrganizationAddress(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addOrganizationAddress);
        return this._Common.postData(url, data);
    };

    addOrganizationContact(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addOrganizationContact);
        return this._Common.postData(url, data);
    };

    syncOfMetricAlertRule(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.syncOfMetricAlertRule, scope, id);
        return this._Common.postData(url);
    }

    addCountriesToOrganization(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addCountriesToOrganization);
        return this._Common.postData(url, data);
    }

    updatePipeline(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updatePipeline);
        return this._Common.putData(url, data);
    };

    changePipelineStatus(configurationId, status) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.changePipelineStatus, configurationId, status);
        return this._Common.putData(url);
    };

    changeDeviceRegistrationStatus(id, status) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.changeDeviceRegistrationStatus, id, status);
        return this._Common.putData(url);
    };

    updateDeviceRegistrationInformation(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateDeviceRegistrationInformation);
        return this._Common.putData(url, data);
    };

    approveDeviceRegistrationInformation(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.approveDeviceRegistrationInformation);
        return this._Common.putData(url, data);
    };

    updateRole(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateRole);
        return this._Common.putData(url, data);
    };

    updateUser(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateUser);
        return this._Common.putData(url, data);
    };

    makeUserActive(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.makeUserActive, id);
        return this._Common.putData(url);
    };

    updateProcessingUnit(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateProcessingUnit);
        return this._Common.putData(url, data);
    };

    activateProcessingUnits(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.activateProcessingUnits, id);
        return this._Common.putData(url);
    };

    deviceRegister(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deviceRegister);
        return this._Common.putData(url, data);
    };

    changePassword(id, password) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.changePassword, id, password);
        return this._Common.putData(url);
    };

    makeUserRoleGlobal(userId, roles) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.makeUserRoleGlobal, userId);
        _.map(roles, function(val) {
            url = url + "&roleId=" + val;
        });
        return this._Common.putData(url);
    };

    updateOrganization(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateOrganization);
        return this._Common.putData(url, data);
    };

    updateOrganizationAddress(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateOrganizationAddress);
        return this._Common.putData(url, data);
    };

    updateOrganizationContact(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.updateOrganizationContact);
        return this._Common.putData(url, data);
    };

    activateOrganizqation(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.activateOrganizqation, id);
        return this._Common.putData(url);
    };

    activateSource(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.activateSource, id);
        return this._Common.putData(url);
    };

    deactivateSource(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deactivateSource, id);
        return this._Common.deleteData(url);
    };

    activateDataType(sourceId, dataTypes) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.activateDataType, sourceId);
        _.map(dataTypes, function(val) {
            url = url + "&sourceFormatToSourceId=" + val;
        });
        return this._Common.putData(url);
    };

    deactivateDataType(sourceId, dataTypes) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deactivateDataType, sourceId);
        _.map(dataTypes, function(val) {
            url = url + "&sourceFormatToSourceId=" + val;
        });
        return this._Common.putData(url);
    };

    metricAlertRuleAddUpdate(data, scope, skipsync) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.metricAlertRuleAddUpdate, scope, skipsync);
        //if(typeof(skipsync) !== 'undefined') {
        //    url = url + '?skipsync=' + skipsync;
        //}
        return this._Common.putData(url, data);
    };

    setMetricAlertRuleSilenced(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.setMetricAlertRuleSilenced, scope, id);
        return this._Common.putData(url);
    }

    markErrorAsRead(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.markErrorAsRead, id);
        return this._Common.putData(url);
    }

    deleteSourceFormatsToSource(id, dataTypes) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteSourceFormatsToSource, id);
        _.map(dataTypes, function(val) {
            url = url + "&sourceFormatToSourceId=" + val;
        });
        return this._Common.deleteData(url);
    };

    logout() {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.logout);
        return this._Common.deleteData(url);
    };

    deleteRole(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteRole, id);
        return this._Common.deleteData(url);
    }

    removeRolesFromOrganizationsAndUser(userId, organizationId, roles) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.removeRolesFromOrganizationsAndUser, userId, organizationId);
        _.map(roles, function(val) {
            url = url + "&roleId=" + val;
        });
        return this._Common.deleteData(url);
    }

    deletePermissionsFromRole(id, permissions) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deletePermissionsFromRole, id);
        _.map(permissions, function(val) {
            url = url + "&permission=" + val;
        });
        return this._Common.deleteData(url);
    }

    makeInactiveActive(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.makeInactiveActive, id);
        return this._Common.deleteData(url);
    };

    deactivateProcessingUnits(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deactivateProcessingUnits, id);
        return this._Common.deleteData(url);
    };

    deactivateOrganizqation(id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deactivateOrganizqation, id);
        return this._Common.deleteData(url);
    };

    deleteMetricAlertRule(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteMetricAlertRule, scope, id);
        return this._Common.deleteData(url);
    }

    setMetricAlertRuleUnsilenced(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.setMetricAlertRuleUnsilenced, scope, id);
        return this._Common.deleteData(url);
    }

    deleteAlerts(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteAlerts, scope, id);
        return this._Common.deleteData(url);
    }

    deleteUserRoleGlobal(userId, roles) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteUserRoleGlobal, userId);
        _.map(roles, function(val) {
            url = url + "&roleId=" + val;
        });
        return this._Common.deleteData(url);
    };

    deleteCountriesToOrganization(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.deleteCountriesToOrganization);
        return this._Common.deleteData(url, data);
    }

    markAlertAsRead(scope, id) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.markAlertAsRead, scope, id);
        return this._Common.deleteData(url);
    }
}

export default ProgressRail;