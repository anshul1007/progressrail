﻿class Mapper  {

    constructor() {
    }

    throughputData(throughputData) {
        var data = [];
        for(var item in throughputData) {
            var label = throughputData[item].key;
            var keys =   Object.keys(throughputData[item].values);
            var values = [];
            for(var val in keys ) {
                var dataKey = keys[val];
                var dataValue = throughputData[item].values[keys[val]];
                values.push([dataKey, dataValue]);
            }
            if(label.indexOf("success") > 0) {
                data.push({ "key": "Success" , "values" :values,"color": "#79973F"});
            } 
            else if(label.indexOf("fail") > 0) {
                data.push({ "key": "Fail" , "values" :values,"color": "#ff0000"});
            }
        }
        return data;
    }
}

export default Mapper;