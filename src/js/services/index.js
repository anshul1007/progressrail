import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);

import ProgressRailService from './progress.rail.service'
servicesModule.service('ProgressRail', ProgressRailService);

import CommonService from './common.service'
servicesModule.service('Common', CommonService);

import MapperService from './mapper.service'
servicesModule.service('Mapper', MapperService);

import UserService from './user.service'
servicesModule.service('User', UserService);

export default servicesModule;
