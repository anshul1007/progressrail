export default class User {
    constructor(AppConstants, ProgressRail, $state, $q, $window) {
        'ngInject';

        this.ProgressRail = ProgressRail;
        this.$state = $state;
        this.$q = $q;
        this.$window = $window;
        this.current = null;
        this.addOrganization = null;
        //this.checkCurrentUser().then(
        //    () => {
        //        if(this.$state.current.name === 'app.login') {
        //            this.$state.go('app.home');
        //        }
        //    }, 
        //    () => {
        //        if(this.$state.current.name !== 'app.login') {
        //            this.$state.go('app.login');
        //        }
        //    });
    }

    checkCurrentUser() {
        var deferred = this.$q.defer();
        this.ProgressRail
            .getCurrentUser()
            .then(
                (res) => {
                    this.current = res;
                    deferred.resolve('');
                }, (error) => {
                    this.current = null;
                    deferred.reject(error);
                });
        return deferred.promise;
    }

    attemptAuth(userName, password) {
        var deferred = this.$q.defer();
        this.ProgressRail
            .login(userName, password)
            .then(
                (res) => {
                    this.current = res;
                    deferred.resolve('');
                    this.$state.go("app.home");
                }, (error) => {
                    this.current = null;
                    deferred.resolve(error);
                });
        return deferred.promise;
    }

    logout() {
        this.ProgressRail
                .logout()
                .then(
                    () => {
                        this.current = null;
                        this.$window.location.reload(true)
                    });
    }

}
