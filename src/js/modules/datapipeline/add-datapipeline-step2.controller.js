import DatapipelineTestCtrl from '../modals/datapipeline-test';

class AddDataPipelineStep2Ctrl {
    constructor($sessionStorage, $state, ProgressRail, $uibModal, $scope, Common) {
        'ngInject';
        this.$scope = $scope;
        this.$storage = $sessionStorage;
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.$uibModal = $uibModal;
        this.Common = Common;

        if (!(this.$storage.dataPipeline && this.$storage.dataPipeline.organization)) {
            this.$state.go('^.step1');
        }
       
        let allDevice = [{ 
            "id": null, 
            "sourceCategoryId": null, 
            "name": "All",
            "description": "All",
            "tags": []
        }];

        this.devices = [];
        this.dataTypes = [];
        this.versions = [];
        this.processingUnits = [];
        this.distributionUnits = [];
        
        if(this.$storage.dataPipeline && !this.$storage.dataPipeline.code) {
            this.$storage.dataPipeline.code =  this.Common.guid();
        }

        this.ProgressRail
            .getSourceByOrganizationForPipelineManage(this.$storage.dataPipeline.organization.id)
            .then(
            (devices) => {
                this.devices = _.union(allDevice, _.sortBy(devices, 'name'));
                if(this.$storage.dataPipeline.device) {
                    this.selectedDevice = _.findWhere(this.devices, {id: this.$storage.dataPipeline.device.id});
                }
                else {
                    this.selectedDevice = _.findWhere(this.devices, {id: allDevice[0].id});
                }
                this.$storage.dataPipeline.device = this.selectedDevice;

                if(this.$storage.dataPipeline.device) {
                    if(this.$storage.dataPipeline.device.id !== null) {
                        this.ProgressRail
                            .getDataTypesbySourceCategory(this.$storage.dataPipeline.device.sourceCategoryId)
                            .then(
                            (dataTypes) => {
                                this.dataTypes = _.sortBy(dataTypes, 'name');
                                if(this.$storage.dataPipeline.dataType) {
                                    this.selectedDataType = _.findWhere(this.dataTypes, {id: this.$storage.dataPipeline.dataType.id});
                                    this.versions = _.uniq(_.pluck(_.filter(this.dataTypes, {"name" : this.$storage.dataPipeline.dataType.name }), "version"));
                                    this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion = _.first(this.versions);
                                }
                            });
                    }
                    else {
                        this.ProgressRail
                                .getDataTypes()
                                .then(
                                (dataTypes) => {
                                    this.dataTypes = _.sortBy(dataTypes, 'name');
                                    if(this.$storage.dataPipeline.dataType) {
                                        this.selectedDataType = _.findWhere(this.dataTypes, {id: this.$storage.dataPipeline.dataType.id});
                                        this.versions = _.uniq(_.pluck(_.filter(this.dataTypes, {"name" : this.$storage.dataPipeline.dataType.name }), "version"));
                                        this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion = _.first(this.versions);
                                    }
                                });
                    }

                }
            });

        this.ProgressRail
          .getProcessingUnitsbyType('processing', true)
          .then(
          (processingUnits) => {
              var data = _.map(processingUnits, 
                  function (units) {
                      units.inputFormatTemplate = JSON.parse(units.inputFormatTemplate);
                      return units;
                  });
              this.processingUnits = data;
          });

        this.ProgressRail
           .getProcessingUnitsbyType('distribution', true)
           .then(
           (distributionUnits) => {
               var data = _.map(distributionUnits, 
                                function (units) {
                                    units.inputFormatTemplate = JSON.parse(units.inputFormatTemplate);
                                    return units;
                                });
               this.distributionUnits = data;
           });

        if(this.$storage.dataPipeline.dataTypeVersion) {
            this.selectedVersion = _.findWhere(this.versions, {id: this.$storage.dataPipeline.dataTypeVersion.id});
        }
    }

    save() {
        let testData =  {
            "organizationId": this.$storage.dataPipeline.organization.id,
            "name": this.$storage.dataPipeline.name,
            "uniqueid": this.$storage.dataPipeline.code,
            "description": this.$storage.dataPipeline.description
        };
        this.error = null;
        this.ProgressRail.addPipeline(testData).then(
                (id) => {
                    if(id) {
                        console.log(id);
                        this.$storage.dataPipeline.id = id;
                    }
                }, (error) => {
                    this.error = error.data;
                });
    }

    update() {
        this.error = null;
        let testData =  {
            "id": this.$storage.dataPipeline.id,
            "organizationId": this.$storage.dataPipeline.organization.id,
            "name": this.$storage.dataPipeline.name,
            "uniqueid": this.$storage.dataPipeline.code,
            "description": this.$storage.dataPipeline.description
        };

        if(this.$storage.dataPipeline.device) {
            testData.sourceId = this.$storage.dataPipeline.device.id;
        }
        else {
            testData.sourceId = null;
        }

        if(this.$storage.dataPipeline.dataType && this.selectedVersion) {
            testData.sourceFormatId = _.find(this.dataTypes, {"name": this.$storage.dataPipeline.dataType.name, "version": this.selectedVersion}).id;
        }
        else {
            testData.sourceFormatId = null;
        }

        let pipelineId = this.$storage.dataPipeline.id;

        if(this.$storage.dataPipeline.processes && this.$storage.dataPipeline.processes.length > 0  && this.$storage.dataPipeline.distribution) {
            this.$storage.dataPipeline.distribution.sequence = this.$storage.dataPipeline.processes.length + 1;
            testData.processingUnitConfigurations = _.map(_.union(this.$storage.dataPipeline.processes, [this.$storage.dataPipeline.distribution]), 
                                              function(process, index) {
                                                  return {pipelineConfigurationId : pipelineId, processingUnitId: process.id, sequence: index + 1, configuration: JSON.stringify(process.inputFormatTemplate)};
                                              });
        }
        else if(this.$storage.dataPipeline.processes && this.$storage.dataPipeline.processes.length > 0) {
            testData.processingUnitConfigurations = _.map(_.union(this.$storage.dataPipeline.processes), 
                                                  function(process, index) {
                                                      return {pipelineConfigurationId : pipelineId, processingUnitId: process.id, sequence: index + 1, configuration: JSON.stringify(process.inputFormatTemplate)};
                                                  });
        }
        else {
            testData.processingUnitConfigurations = null;
        }

        this.error = null;

        this.ProgressRail.updatePipeline(testData).then(
                (result) => {
                    if(result && result.validationErrors && result.validationErrors.length > 0) {
                        this.error = result.validationErrors;
                    }
                    else {
                        this.$storage.dataPipeline.status = result.approvalStatus;
                        //console.log(result);
                    }
                }, (error) => {
                    this.error = error.data;
                });
    }

    selectDevice($item, $model, $label) {
        this.$storage.dataPipeline.device = $item;
        if(this.$storage.dataPipeline.device.id !== null) {
            this.ProgressRail
                .getDataTypesbySourceCategory(this.$storage.dataPipeline.device.sourceCategoryId)
                .then(
                (dataTypes) => {
                    this.dataTypes = _.sortBy(dataTypes, 'name');
                    if(this.$storage.dataPipeline.dataType) {
                        this.selectedDataType = _.findWhere(this.dataTypes, {id: this.$storage.dataPipeline.dataType.id});
                        this.versions = _.uniq(_.pluck(_.filter(this.dataTypes, {"name" : this.$storage.dataPipeline.dataType.name }), "version"));
                        this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion = _.first(this.versions);
                    }
                });
        }
        else {
            this.ProgressRail
                    .getDataTypes()
                    .then(
                    (dataTypes) => {
                        this.dataTypes = _.sortBy(dataTypes, 'name');
                        if(this.$storage.dataPipeline.dataType) {
                            this.selectedDataType = _.findWhere(this.dataTypes, {id: this.$storage.dataPipeline.dataType.id});
                            this.versions = _.uniq(_.pluck(_.filter(this.dataTypes, {"name" : this.$storage.dataPipeline.dataType.name }), "version"));
                            this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion = _.first(this.versions);
                        }
                    });
        }
        this.update();
    }
    
    removeDevice() {
        this.$storage.dataPipeline.device = this.selectedDevice = null;
        this.removeDataType();
    }

    selectDataType($item, $model, $label) {
        this.$storage.dataPipeline.dataType = $item;
        this.versions = _.uniq(_.pluck(_.filter(this.dataTypes, {"name" : this.$storage.dataPipeline.dataType.name }), "version"));
        this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion = _.first(this.versions);
        this.update();
    }

    removeDataType() {
        this.$storage.dataPipeline.dataType =  this.selectedDataType = null;
        this.$storage.dataPipeline.dataTypeVersion =  this.selectedVersion = null;
        this.$storage.dataPipeline.processes = null;
        this.$storage.dataPipeline.distribution = null;
        this.update();
    }

    selectVersion() {
        this.$storage.dataPipeline.dataTypeVersion = this.selectedVersion;
        this.update();
    }

    selectProcess() {
        this.closePopup();
        this.showProcessStep1 = true;
    }

    addProcess(process) {
        this.selectedProcess = process
        this.closePopup();
        this.showProcessStep2 = true;
    }

    submitProcess() {
        if(!this.$storage.dataPipeline.processes) {
            this.$storage.dataPipeline.processes = [];
        }
        this.$storage.dataPipeline.processes.push({ id : this.selectedProcess.id, name : this.selectedProcess.name, inputFormatTemplate : this.selectedProcess.inputFormatTemplate, sequence :  this.$storage.dataPipeline.processes.length + 1});
        this.closePopup();
        this.update();
    }

    editProcess(sequence) {
        this.closePopup();
        this.selectedProcess = _.find(this.$storage.dataPipeline.processes, {"sequence": sequence});
        this.edit = true;
        this.showProcessStep2 = true;  
    }

    updateProcess() {
        this.closePopup();
        this.update();
    }

    removeProcess(index) {
        this.$storage.dataPipeline.processes.splice(index, 1);
        this.update();
    }

    selectDistribution() {
        this.closePopup();
        this.showDistribution1 = true;
    }

    addDistribution(distribution) {
        this.selectedDistribution = distribution
        this.closePopup();
        this.showDistribution2 = true;
    }

    submitDistribution() {
        this.$storage.dataPipeline.distribution = { id : this.selectedDistribution.id, name : this.selectedDistribution.name, inputFormatTemplate : this.selectedDistribution.inputFormatTemplate, sequence : 1};
        this.closePopup();
        this.update();
    }

    editDistribution(sequence) {
        this.closePopup();
        this.selectedDistribution = this.$storage.dataPipeline.distribution;
        this.edit = true;
        this.showDistribution2 = true;  
    }

    updateDistribution() {
        this.closePopup();
        this.update();
    }

    removeDistribution() {
        this.$storage.dataPipeline.distribution = null;
        this.update();
    }

    closePopup(){
        this.showProcessStep1 = false;
        this.showProcessStep2 = false;
        this.showDistribution1 = false;
        this.showDistribution2 = false;
        this.edit = false;
    }

    //setUpTest(s3Url) {
    //    this.ProgressRail
    //        .testPipeline(this.$storage.dataPipeline.id, s3Url)
    //        .then(
    //        (result) => {
    //            alert(1);
    //        });
    //}

    testDatapipeline() {
        this.update();
        const  modalInstance = this.$uibModal.open({
            templateUrl: 'modules/modals/datapipeline-test.html',
            controller: DatapipelineTestCtrl,
            scope: this.$scope,
            controllerAs: '$ctrl',
            resolve: {
                dataPipelineId: () => this.$storage.dataPipeline.id
            }
        });
    }
}

export default AddDataPipelineStep2Ctrl;
