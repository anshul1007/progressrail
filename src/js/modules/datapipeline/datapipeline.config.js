function AddDataPipelineConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.adddatapipeline', {
            abstract: true,
            url: '/adddatapipeline',
            controller: 'AddDataPipelineCtrl',
            controllerAs: '$ctrl',
            resolve: {
            },
            templateUrl: 'modules/datapipeline/add-datapipeline.html'
        })
        .state('app.adddatapipeline.step1', {
            url: '/step1',
            controller: 'AddDataPipelineStep1Ctrl',
            resolve: {
                organizations: function($sessionStorage, $q, ProgressRail) {
                    var deferred = $q.defer();
                    ProgressRail
                      .getOrganizationForPipelineManage()
                      .then(
                        (organizations) => {
                            $sessionStorage.organizations = organizations;
                            deferred.resolve($sessionStorage.organizations);
                        });
                    return deferred.promise;
                }
            },
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/add-datapipeline-step1.html',
            title: 'Add DataPipeline: Step 1'
        })
        .state('app.adddatapipeline.step2', {
            url: '/step2',
            controller: 'AddDataPipelineStep2Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/add-datapipeline-step2.html',
            title: 'Add DataPipeline: Step 2'
        })    
        .state('app.datapipeline', {
            url: '/datapipeline',
            controller: 'DatapipelineCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/datapipeline.html',
            resolve: {
                organizations: function($q, ProgressRail) {
                    var deferred = $q.defer();
                    ProgressRail
                      .getOrganizationForPipelineView()
                      .then(
                        (organizations) => {
                            deferred.resolve(organizations);
                        });
                    return deferred.promise;
                },
                devices : function($q, ProgressRail) {
                    var deferred = $q.defer();
                    deferred.resolve([]);
                    return deferred.promise;
                },
                dataTypes : function($q, ProgressRail) {
                    var deferred = $q.defer();
                    ProgressRail
                      .getDataTypes()
                      .then(
                        (dataTypes) => {
                            deferred.resolve(dataTypes);
                        });
                    return deferred.promise;
                },
                statuses: function($q, ProgressRail) {
                    var deferred = $q.defer();
                    ProgressRail
                      .getPipelineStatuses()
                      .then(
                        (statuses) => {
                            deferred.resolve(statuses);
                        });
                    return deferred.promise;
                }
            }
        })
        .state('app.datapipeline-history', {
            url: '/datapipeline-history/:datapipelineId/:showLastResult',
            controller: 'DatapipelineHistroyCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/datapipeline-history.html',
            title: 'Datapipeline History'
        })
        .state('app.processingunit', {
            url: '/processingunit',
            controller: 'ProcessingUnitCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/processing-unit.html'
        })
        .state('app.processingunitdetails', {
            url: '/processingunitdetails/:processingunitid',
            controller: 'ProcessingUnitDetailsCtrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/datapipeline/processing-unit-details.html',
        });

};

export default AddDataPipelineConfig;
