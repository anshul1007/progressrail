import ProcessingUnitDetailsCtrl from '../modals/processing-unit-details.controller';

class ProcessingUnitCtrl {
    constructor(ProgressRail, $uibModal, $sessionStorage, $scope) {
        'ngInject';
      
        this.ProgressRail = ProgressRail;
        this.$uibModal = $uibModal;
        this.$storage = $sessionStorage;
        this.$scope = $scope;
        this.items = [];

        this.ProgressRail
                 .getProcessingUnitTypes()
                 .then(
                   (processingUnitTypes) => {
                       this.$storage.processingUnitTypes = processingUnitTypes;
                   });

        this.searchFilter = {
            processingUnitType : null,
            className : null,
            status : null,
            name : null,
            description : null
        }

        this.$scope.totalItems = 0;
        this.$scope.currentPage = 1;
        this.$scope.maxSize = 10;
        this.$scope.itemsPerPage = 25;

        this.search();
    }

    pageChanged() {
        this.search();
    };

    addNew() {
        this.showDetails();
    }

    search() {
        this.ProgressRail
                .searchProcessingUnits(this.$scope.currentPage - 1, this.$scope.itemsPerPage, 
                    this.searchFilter.processingUnitType, this.searchFilter.className, this.searchFilter.status, 
                    this.searchFilter.name, this.searchFilter.description)
                .then(
                   (response) => {
                       this.$scope.totalItems = response.totalHits;
                       this.items = response.result;
                   });
    }
    
    edit(processingUnit) {
        this.showDetails(processingUnit);
    }

    showDetails(processingUnit) {
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/processing-unit-details.html',
            controller: ProcessingUnitDetailsCtrl,
            resolve: {
                processingUnit: function() {
                    return processingUnit;
                },
                processingUnitTypes : function ($q, $sessionStorage, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.processingUnitTypes) {
                        deferred.resolve($sessionStorage.processingUnitTypes);
                    }
                    else {
                        ProgressRail
                             .getProcessingUnitTypes()
                             .then(
                               (processingUnitTypes) => {
                                   $sessionStorage.processingUnitTypes = processingUnitTypes;
                                   deferred.resolve($sessionStorage.processingUnitTypes);
                               });
                    }
                    return deferred.promise;
                }
            },
            controllerAs: '$ctrl',
        });
    }

    enable(item) {
        this.error = "";
        this.ProgressRail
             .activateProcessingUnits(item.id)
             .then(
               (result) => {
                   item.status = true;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

    disable(item) {
        this.error = "";
        this.ProgressRail
             .deactivateProcessingUnits(item.id)
             .then(
               (result) => {
                   item.status = false;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }
}


export default ProcessingUnitCtrl;
