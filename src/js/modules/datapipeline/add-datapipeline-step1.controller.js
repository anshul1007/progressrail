class AddDataPipelineStep1Ctrl {
    constructor($sessionStorage, $state, $scope, organizations) {
        'ngInject';
        this.$storage = $sessionStorage;
        this.$scope = $scope;
        this.$state = $state;
        this.organizations = _.sortBy(organizations, function (i) { return i.name.toLowerCase(); });
        
        this.selectedOrganization = null;

        this.$storage.dataPipeline = {
            organization : null
        };

    }

    selectOrganization() {
        this.$storage.dataPipeline.organization = this.selectedOrganization;
        this.$state.go('^.step2');
    }

}

export default AddDataPipelineStep1Ctrl;
