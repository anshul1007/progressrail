import angular from 'angular';

// Create the module where our functionality can attach to
let datapipelineModule = angular.module('app.datapipeline', []);

// Include our UI-Router config settings
import AddDataPipelineConfig from './datapipeline.config';
datapipelineModule.config(AddDataPipelineConfig);

// Controllers
import AddDataPipelineCtrl from './add-datapipeline.controller';
datapipelineModule.controller('AddDataPipelineCtrl', AddDataPipelineCtrl);

import  AddDataPipelineStep1Ctrl from './add-datapipeline-step1.controller';
datapipelineModule.controller('AddDataPipelineStep1Ctrl',  AddDataPipelineStep1Ctrl);

import  AddDataPipelineStep2Ctrl from './add-datapipeline-step2.controller';
datapipelineModule.controller('AddDataPipelineStep2Ctrl',  AddDataPipelineStep2Ctrl);

import DatapipelineCtrl from './datapipeline.controller';
datapipelineModule.controller('DatapipelineCtrl', DatapipelineCtrl);

import DatapipelineHistroyCtrl from './datapipeline-histroy.controller';
datapipelineModule.controller('DatapipelineHistroyCtrl', DatapipelineHistroyCtrl);

import ProcessingUnitCtrl from './processing-unit.controller';
datapipelineModule.controller('ProcessingUnitCtrl', ProcessingUnitCtrl);

export default datapipelineModule;
