
class DatapipelineCtrl {
    constructor($state, ProgressRail, $scope, $sessionStorage, organizations, devices, statuses, dataTypes) {
        'ngInject';
        this.$scope = $scope;
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.$sessionStorage = $sessionStorage;
        this.organizations = organizations;
        this.devices = devices;
        this.dataTypes = dataTypes || [];
        this.statuses = statuses;

        this.items = [];

        this.searchFilter = {
            selectedOrganization : null,
            selectedDevice : null,
            selectedDataType : null,
            dataPipelineName : null,
            status : 'READY_FOR_APPROVAL'
        };

        this.refreshDeviceList();

        this.$scope.totalItems = 0;
        this.$scope.currentPage = 1;
        this.$scope.maxSize = 10;
        this.$scope.itemsPerPage = 25;

        this.search();
    }

    pageChanged() {
        this.search();
    };

    edit(id) {
        this.ProgressRail
                 .getPipelineById(id)
                 .then(
                 (result) => {
                     var distribution = _.findWhere(result.processingUnitConfigurations , { "processingUnitType" : "distribution"});
                     this.$sessionStorage.dataPipeline = {
                         id: result.id,
                         organization : {
                             id: result.organizationId,
                             name: result.organizationName
                         },
                         code : result.uniqueId,
                         name: result.name,
                         description: result.description,
                         device : result.sourceId,
                         status : result.approvalStatus,
                         dataType : {
                             name : result.sourceFormatName,
                             dataTypeVersion: result.sourceFormatVersion
                         },
                         processes : _.map(_.where(result.processingUnitConfigurations , { "processingUnitType" : "processing"}), 
                                              function(process, index) {
                                                  return { 
                                                      id : process.processingUnitId, 
                                                      name : process.processingUnitName,
                                                      sequence: process.sequence, 
                                                      inputFormatTemplate: JSON.parse(process.configuration) 
                                                  };
                                              }),
                         distribution : (distribution)
                                        ? ({
                                            id : distribution.processingUnitId, 
                                            name : distribution.processingUnitName, 
                                            sequence: distribution.sequence, 
                                            inputFormatTemplate: JSON.parse(distribution.configuration)
                                        })
                                        : null
                     };
                     this.$state.go('app.adddatapipeline.step2');
                 });
    }

    approve(datapipeline) {
        this.ProgressRail
            .changePipelineStatus(datapipeline.id, 'APPROVED')
            .then(
            (result) => {
                datapipeline.approvalStatus = 'APPROVED';
                //this.ProgressRail
                //    .lastTestResultForPipeline(datapipeline.id)
                //    .then((result) => {
                              
                //    });
            });
    }

    selectOrganization($item, $model, $label) {
        this.searchFilter.selectedOrganization = $item;
        this.ProgressRail
            .getSourceByOrganizationForPipelineView(this.searchFilter.selectedOrganization.id)
            .then(
            (devices) => {
                this.devices = devices;
            });

        this.refreshDeviceList(this.searchFilter.selectedOrganization.id);
    }

    refreshDeviceList(argOrgId) {

        let orgId = ' ';

        if(argOrgId && argOrgId.trim() !== "") {
            orgId = argOrgId;
        }

        this.ProgressRail
            .getSourceByOrganizationForPipelineView(orgId)
            .then(
                (devices) => {
                    this.devices = devices;
                });
    }

    removeOrganization() {
        this.searchFilter.selectedOrganization = null;
        this.selectedOrganization = null;
        // this.removeDevice();
        this.refreshDeviceList();
    }

    selectDevice($item, $model, $label) {
        this.searchFilter.selectedDevice = $item;
        //this.searchFilter.selectedDataType = null;
        //this.selectedDataType = null;
        //if(this.searchFilter.selectedDevice) {
        //    this.ProgressRail
        //        .getSourceFormatsBySource(this.searchFilter.selectedDevice.id)
        //        .then(
        //        (dataTypes) => {
        //            this.dataTypes = dataTypes;
        //        });
        //}
        //else {
        //    this.dataTypes = null;
        //}
    }

    removeDevice() {
        this.searchFilter.selectedDevice = null;
        this.selectedDevice = null;
        // this.refreshDeviceList();
        //this.searchFilter.selectedDataType = null;
        //this.selectedDataType = null;
    }

    selectDataType($item, $model, $label) {
        this.searchFilter.selectedDataType = $item;
    }

    removeDataType() {
        this.searchFilter.selectedDataType = null;
        this.selectedDataType = null;
    }

    search() {
        this.ProgressRail
                .searchPipelinePaged(this.$scope.currentPage - 1, this.$scope.itemsPerPage, 
                    (this.searchFilter.selectedOrganization) ? this.searchFilter.selectedOrganization.id : null, 
                    (this.searchFilter.selectedDevice) ? this.searchFilter.selectedDevice.id : null, 
                    (this.searchFilter.selectedDataType) ? this.searchFilter.selectedDataType.id : null, 
                    this.searchFilter.dataPipelineName, this.searchFilter.status)
                .then(
               (response) => {
                   this.$scope.totalItems = response.totalHits;
                   this.items = response.result;
               });
    }
}


export default DatapipelineCtrl;
