﻿
class DatapipelineHistroyCtrl {
    constructor(ProgressRail, $stateParams, $sessionStorage, $state) {
        'ngInject';
        this.ProgressRail = ProgressRail;
        this.$stateParams = $stateParams;
        this.$sessionStorage = $sessionStorage;
        this.$state = $state;

        this.history = [];

        if(!this.$stateParams.showLastResult) {
            this.getPipelineHistory();
        } else {
        
        }
    }

    getTotalDuration(items) {
        return  _.reduce(items, function(sum, obj){ return sum + obj.duration; }, 0); 
    }

    getPipelineHistory() {
        this.ProgressRail
                  .getAllTestResultForPipeline(this.$stateParams.datapipelineId)
                  .then(
                    (history) => {
                        this.history = history;
                    });
    }

    downloadPipelineTestOutput(s3Url) {
        this.ProgressRail
                     .downloadPipelineTestOutput(s3Url)
                     .then(
                       (text) => {
                           var blob = new Blob([JSON.stringify(text)], {type: "text/plain;charset=utf-8"});
                           saveAs(blob, s3Url+".txt");
                       });
    }

    toDate(utcSeconds) {
        var d = new Date(0);
        return new Date(d.setUTCSeconds(utcSeconds));
    }

    edit() {
        this.ProgressRail
                 .getPipelineById(this.$stateParams.datapipelineId)
                 .then(
                 (result) => {
                     var distribution = _.findWhere(result.processingUnitConfigurations , { "processingUnitType" : "distribution"});
                     this.$sessionStorage.dataPipeline = {
                         id: result.id,
                         organization : {
                             id: result.organizationId,
                             name: result.organizationName
                         },
                         code : result.uniqueId,
                         name: result.name,
                         description: result.description,
                         device : result.sourceId,
                         status : result.approvalStatus,
                         dataType : {
                             name : result.sourceFormatName,
                             dataTypeVersion: result.sourceFormatVersion
                         },
                         processes : _.map(_.where(result.processingUnitConfigurations , { "processingUnitType" : "processing"}), 
                                              function(process, index) {
                                                  return { 
                                                      id : process.processingUnitId, 
                                                      name : process.processingUnitName,
                                                      sequence: process.sequence, 
                                                      inputFormatTemplate: JSON.parse(process.configuration) 
                                                  };
                                              }),
                         distribution : (distribution)
                                        ? ({
                                            id : distribution.processingUnitId, 
                                            name : distribution.processingUnitName, 
                                            sequence: distribution.sequence, 
                                            inputFormatTemplate: JSON.parse(distribution.configuration)
                                        })
                                        : null
                     };
                     this.$state.go('app.adddatapipeline.step2');
                 });
    }
}


export default DatapipelineHistroyCtrl;
