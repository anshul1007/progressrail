﻿class AddTagCtrl {
    constructor($uibModalInstance, ProgressRail, $sessionStorage) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;
        this.$storage = $sessionStorage;

        this.model = {
            name : "",
            validate : false
        }
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        this.model.validate = true;
        if(form.$valid) {
            var data = {
                "name": this.model.name, 
            }
            // add tag
            this.ProgressRail
              .addTag(data)
              .then(
                (id) => {
                    if(id) {
                        this.$storage.tags.push({ "id": id, "name": data.name });
                        this.$uibModalInstance.dismiss();
                    }
                });
        }
    }
}

export default AddTagCtrl;