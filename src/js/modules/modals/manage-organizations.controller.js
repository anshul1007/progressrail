﻿class ManageOrganizationsCtrl {
    constructor($q, $uibModalInstance, ProgressRail, organizations, selectedOrganizations, userId, roles) {
        'ngInject';
        this.$q = $q;
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;
        this.organizations = _.filter(organizations, function(org){ return !_.findWhere(selectedOrganizations, { "id": org.id}) });
        this.userId = userId;
        this.roles = roles;
        this.selectedRoles = [];
        this.searchFilter = {
            selectedOrganization : null
        };
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    selectOrganization($item, $model, $label) {
        this.searchFilter.selectedOrganization = $item;
    }

    removeOrganization() {
        this.searchFilter.selectedOrganization = null;
        this.selectedOrganization = null;
    }

    selectRole(role) {
        var idx = _.findWhere(this.selectedRoles, {id : role.id});
        if (idx) {
            this.selectedRoles = _.reject(this.selectedRoles, {id : role.id});
        }
        else {
            this.selectedRoles.push({id: role.id, name : role.roleName});
        }
    }

    checkRole(role) {
        return  _.findWhere(this.selectedRoles, {id : role.id});
    }

    save() {
        this.error = "";
        var that = this;
        if(this.selectedRoles.length > 0) {
            this.ProgressRail.addRolesToOrganizationsAndUser(this.userId, this.searchFilter.selectedOrganization.id, _.pluck(this.selectedRoles, "id"))
                  .then(
                    (data) => {
                        that.$uibModalInstance.dismiss();
                    }, (error) => {
                        this.error = error.data;
                    });
        }
        else {
            this.error = "Please select at least one role to save organization.";
        }
    }
}

export default ManageOrganizationsCtrl;