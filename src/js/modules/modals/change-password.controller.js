﻿
class ChangePasswordCtrl {
    constructor($uibModalInstance, $uibModal, ProgressRail, userId) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;

        this.model = {
            userId : userId,
            password : "",
            confirmPassword: "", 
        }
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    update(form) {
        this.model.validate = true;
        if(form.$valid && this.model.password === this.model.confirmPassword) {
            this.ProgressRail
              .changePassword(this.model.userId, this.model.password)
              .then(
                (id) => {
                    this.$uibModalInstance.dismiss();
                });
        }
    }
}

export default ChangePasswordCtrl;