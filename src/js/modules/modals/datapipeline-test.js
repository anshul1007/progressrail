﻿
class DatapipelineTestCtrl {
    constructor($uibModalInstance, $uibModal, $scope, $sessionStorage, $state, ProgressRail, dataPipelineId) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.$uibModal = $uibModal;
        this.$scope = $scope;
        this.$storage = $sessionStorage;
        this.ProgressRail = ProgressRail;
        this.$state = $state;
        this.model = {
            url : '',
            dataPipelineId : dataPipelineId,
            step: 1,
            processingUnitTestResults : [],
            status : ''
        };
    }

    close() {
        this.$uibModalInstance.dismiss();
    }

    setUpTest(s3Url) {
        this.ProgressRail
            .testPipeline(this.model.dataPipelineId, s3Url)
            .then(
            (result) => {
                this.model.step = 2; 
                if(result) {
                    this.model.processingUnitTestResults = result;
                    this.model.status = result.approvalStatus;
                }
            });
    }

    submitForTest(form) {
        this.model.validate = true;
        if(form.$valid) {
            //this.$scope.$parent.$ctrl.setUpTest(this.model.url);
            this.setUpTest(this.model.url);
        }
    }

    submitForApproval() {
        this.ProgressRail
                .changePipelineStatus(this.model.dataPipelineId, 'READY_FOR_APPROVAL')
                .then(
                (result) => {
                    this.$uibModalInstance.dismiss();
                    this.$state.go('app.datapipeline');
                });
    }

    downloadPipelineTestOutput(s3Url) {
        this.ProgressRail
                     .downloadPipelineTestOutput(s3Url)
                     .then(
                        (text) => {
                            var blob = new Blob([JSON.stringify(text)], {type: "text/plain;charset=utf-8"});
                            saveAs(blob, s3Url+".txt");
                        });
    }

    toDate(utcSeconds) {
        var d = new Date(0);
        return new Date(d.setUTCSeconds(utcSeconds));
    }

    getTotalDuration(items) {
        return  _.reduce(items, function(sum, obj){ return sum + obj.duration; }, 0); 
    }

}

export default DatapipelineTestCtrl;