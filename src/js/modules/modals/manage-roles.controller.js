﻿class ManageRolesCtrl {
    constructor($q, $uibModalInstance, ProgressRail, roles, selectedRoles, userId, organizationId, global, organizationCode) {
        'ngInject';
        this.$q = $q;
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;
        this.roles = roles;
        this.selectedRoles = selectedRoles || [];
        this.assignedRoles = angular.copy(selectedRoles || []);
        this.userId = userId;
        this.organizationId = organizationId;
        this.global = global;
        this.organizationCode = (global) ? 'Global' : organizationCode;
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    selectRole(role) {
        var idx = _.findWhere(this.selectedRoles, {id : role.id});
        if (idx) {
            this.selectedRoles = _.reject(this.selectedRoles, {id : role.id});
        }
        else {
            this.selectedRoles.push({id: role.id, name : role.name});
        }
    }

    checkRole(role) {
        return  _.findWhere(this.selectedRoles, {id : role.id});
    }

    updateRole() {
        let deferred = this.$q.defer();
        var that = this;

        if(this.global) {
            let promises = [];
            var newRoles = _.reject(this.selectedRoles, function(role){ return _.findWhere(this.assignedRoles, {id:role.id}) }, this);
            if(newRoles.length > 0) {
                let newRolesPromise = this.ProgressRail.makeUserRoleGlobal(this.userId, _.pluck(newRoles, "id"))
                      .then(
                        (data) => {
                        }, (error) => {
                            this.error = error.data;
                        });
                promises.push(newRolesPromise);
            }
            var deleteRoles = _.reject(this.assignedRoles, function(role){ return _.findWhere(this.selectedRoles, {id:role.id}) }, this);
            if(deleteRoles.length > 0) {
                let deleteRolesPromise = this.ProgressRail.deleteUserRoleGlobal(this.userId, _.pluck(deleteRoles, "id"))
                      .then(
                        (data) => {
                        }, (error) => {
                            this.error = error.data;
                        });
                promises.push(deleteRolesPromise);
            }
            if(promises.length > 0) {
                this.$q.all(promises).then(function(result) {
                    deferred.resolve();
                    that.$uibModalInstance.dismiss();
                });
            }
            else {
                deferred.resolve();
                this.$uibModalInstance.dismiss();
            }
        }
        else {
            let promises = [];
            var newRoles = _.reject(this.selectedRoles, function(role){ return _.findWhere(this.assignedRoles, {id:role.id}) }, this);
            if(newRoles.length > 0) {
                let newRolesPromise = this.ProgressRail.addRolesToOrganizationsAndUser(this.userId, this.organizationId, _.pluck(newRoles, "id"))
                      .then(
                        (data) => {
                        }, (error) => {
                            this.error = error.data;
                        });
                promises.push(newRolesPromise);
            }
            var deleteRoles = _.reject(this.assignedRoles, function(role){ return _.findWhere(this.selectedRoles, {id:role.id}) }, this);
            if(deleteRoles.length > 0) {
                let deleteRolesPromise = this.ProgressRail.removeRolesFromOrganizationsAndUser(this.userId, this.organizationId, _.pluck(deleteRoles, "id"))
                      .then(
                        (data) => {
                        }, (error) => {
                            this.error = error.data;
                        });
                promises.push(deleteRolesPromise);
            }
            if(promises.length > 0) {
                this.$q.all(promises).then(function(result) {
                    deferred.resolve();
                    that.$uibModalInstance.dismiss();
                });
            }
            else {
                deferred.resolve();
                this.$uibModalInstance.dismiss();
            }
        }
        return deferred.promise;
    }
}

export default ManageRolesCtrl;