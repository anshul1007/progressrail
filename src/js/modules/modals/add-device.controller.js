﻿class AddDeviceCtrl {
    constructor($uibModalInstance, sourceCategories, $rootScope, ProgressRail, $state) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.sourceCategories = sourceCategories;
        this.$rootScope = $rootScope;
        this.ProgressRail = ProgressRail;
        this.$state =$state;
      
        this.model = {
            code : "",
            description : "",
            uniqueId : "",
            validate : false
        }

        this.model.code = this.$rootScope.organizationCode;
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        this.error = null;
        this.model.validate = true;
        if(form.$valid) {
            var data = {
                "organizationId": this.$rootScope.organizationId,
                "sourceCategoryId":  this.model.dataSourceCategory, 
                "name": this.model.code,
                "description":  this.model.description, 
                "uniqueId": this.uniqueId,
                "status": true 
            }
            // add device
            this.ProgressRail
              .addDevice(data)
              .then(
                (id) => {
                    if(id){
                        this.$state.reload();
                    }
                    this.$uibModalInstance.dismiss();
                },(error) => {
                    this.error = error.data;
                });
        }
    }
}

export default AddDeviceCtrl;