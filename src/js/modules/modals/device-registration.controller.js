﻿
class DeviceRegistrationCtrl {
    constructor($uibModalInstance, $uibModal, ProgressRail, $state, Common) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.$uibModal = $uibModal;
        this.ProgressRail = ProgressRail;
        this.Common = Common;
        this.$state = $state;
        this.error = "";

        this.model = {
            deviceId : "",
            registrationId :  this.Common.guid(),
        }
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        this.model.validate = true;
        if(form.$valid) {
            var data = {
                "identifier": this.model.deviceId.toLowerCase(),
                "requestId": this.model.registrationId.toLowerCase(),
            }
            // device registration
            this.ProgressRail
              .deviceRegister(data)
              .then(
                (result) => {
                    if(result.registrationRequestId) {
                        this.error = "";
                        this.$uibModalInstance.dismiss();
                        this.$state.go('app.deviceregistrationapproval', {deviceRegistrationRequestId : result.registrationRequestId});
                        //this.$state.reload();
                    }
                }, (error) => {
                      console.log("received error = " + JSON.stringify(error));
                      this.error = "Unable to complete your request at this time. Please try again later!";
                  });
        }
    }
}

export default DeviceRegistrationCtrl;