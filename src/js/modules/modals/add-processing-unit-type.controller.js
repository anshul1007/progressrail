﻿class AddProcessingUnitTypeCtrl {
    constructor($uibModalInstance, ProgressRail, $sessionStorage) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;
        this.$storage = $sessionStorage;

        this.model = {
            type : "",
            description: "",
            validate : false
        }
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        this.model.validate = true;
        if(form.$valid) {
            var data = {
                "type": this.model.type,
                "description" : this.model.description
            }
            // add tag
            this.ProgressRail
              .addProcessingUnitType(data)
              .then(
                (id) => {
                    if(id) {
                        this.$storage.processingUnitTypes.push({ "id": id, "type": data.type, "description" : data.description});
                        this.$uibModalInstance.dismiss();
                    }
                });
        }
    }
}

export default AddProcessingUnitTypeCtrl;