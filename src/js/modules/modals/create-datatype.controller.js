﻿import AddTagCtrl from '../modals/add-tag.controller';

class CreateDatatypeCtrl {
    constructor($uibModalInstance, $uibModal, sourceCategories, tags, $rootScope, ProgressRail, $state, $sessionStorage, sourceFormatTypes, presentment) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.$uibModal = $uibModal;
        this.$rootScope = $rootScope;
        this.ProgressRail = ProgressRail;
        this.$state = $state;
        this.$storage = $sessionStorage;

        this.$storage.tags = tags;
        this.sourceCategories = sourceCategories;
        this.sourceFormatTypes = sourceFormatTypes;
        this.mode = presentment.mode;
        
        if(presentment.mode == 'create') {
            this.model = {
                name : "",
                version : "v1.0",
                description : "",
                selectedTags : [],
                validate : false,
                sourceFormatType : sourceFormatTypes[0]
            }
            this.modeLabel = 'Create Datatype';
        } else if(presentment.mode == 'update') {
            console.log('probably update presentment mode...  = ' + JSON.stringify(presentment));

            console.log("sourceCategories = " + JSON.stringify(sourceCategories));
            _.each(sourceCategories, function(sourceCategory){
                if(sourceCategory.name == presentment.selectedDataType.sourceCategoryName) {
                    presentment.selectedDataType.dataSourceCategory = sourceCategory.id;
                }
            });
            this.modeLabel = 'Update Datatype';
            this.model = presentment.selectedDataType;
        }
    }
    
    close() {
        this.$uibModalInstance.dismiss({'result':'none'});
    }

    selectTag (item) {
        if(_.where(this.model.selectedTags, {id: item.id}).length === 0) {
            this.model.selectedTags.push(item);
        }
        this.selectedTag = null;
    }

    removeTag (id) {
        if(_.where(this.model.selectedTags, {id: id}).length !== 0) {
            this.model.selectedTags = _.reject(this.model.selectedTags, function(objArr){ return objArr.id == id; });
        }
    }

    addTag(){
        var modalInstance = this.$uibModal.open({
            templateUrl: 'modules/modals/add.tag.html',
            controller: AddTagCtrl,
            controllerAs: '$ctrl'
        });
    }

    next(form) {
        this.model.validate = true;
        if(form.$valid) {
            this.selectedTab = 1;
        }
    }

    save(form) {
        
        if(this.mode == 'create') {
            this.model.validate = true;
            if(form.$valid) {
                var data = {
                    "sourceCategoryId": this.model.dataSourceCategory,
                    "name": this.model.name.toLowerCase(),
                    "version": this.model.version,
                    "tags":  _.pluck(this.model.selectedTags, "id"),
                    "description":  this.model.description,
                    "sourceFormatType" : this.model.sourceFormatType
                };
                // add data type
                this.ProgressRail
                    .addDataType(data)
                    .then(
                        (id) => {
                            if(id) {
                                this.$uibModalInstance.close({'result':'created'});
                            }
                        });
            }
            else {
                this.selectedTab = 0;
            }
        } else {
            this.model.validate = true;
            if(form.$valid) {
                var data = {
                    "id" : this.model.id,
                    "sourceCategoryId": this.model.dataSourceCategory,
                    "name": this.model.name.toLowerCase(),
                    "version": this.model.version,
                    "tags":  _.pluck(this.model.selectedTags, "id"),
                    "description":  this.model.description,
                    "sourceFormatType" : this.model.sourceFormatType
                };
                // update data type
                this.ProgressRail
                    .updateDataType(data)
                    .then(() => {
                            this.$uibModalInstance.close({'result':'updated'});
                        });
            }
            else {
                this.selectedTab = 0;
            }
        }
    }
}

export default CreateDatatypeCtrl;