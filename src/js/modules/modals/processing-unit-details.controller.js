import AddProcessingUnitTypeCtrl from '../modals/add-processing-unit-type.controller';

class ProcessingUnitDetailsCtrl {
    constructor($uibModalInstance, ProgressRail, processingUnitTypes, $uibModal, $sessionStorage, processingUnit, $state) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.ProgressRail = ProgressRail;
        this.$storage = $sessionStorage;
        this.$uibModal = $uibModal;
        this.$state = $state;
        this.model = {};

        if(processingUnit) {
            this.model = angular.copy(processingUnit);
            this.model.inputFormatTemplate = JSON.parse(this.model.inputFormatTemplate);
        }
    }  

    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        this.error = null;
        this.model.validate = true;
        if(form.$valid && JSON.stringify(this.model.inputFormatTemplate)) {
            var data = { 
                "name": this.model.name,
                "className": this.model.className, 
                "description": this.model.description,
                "inputFormatTemplate":  JSON.stringify(this.model.inputFormatTemplate),
                "type": this.model.type
            };
            if(!this.model.id) {
                // add device
                this.ProgressRail
                  .addProcessingUnit(data)
                  .then(
                    (id) => {
                        this.$uibModalInstance.dismiss();
                        this.$state.reload();
                    },(error) => {
                        this.error = error.data;
                    });
            }
            else {
                // update device
                data.id = this.model.id;
                this.ProgressRail
                  .updateProcessingUnit(data)
                  .then(
                    (id) => {
                        this.$uibModalInstance.dismiss();
                        this.$state.reload();
                    },(error) => {
                        this.error = error.data;
                    });
            }
        }
    }

    addProcessingUnitType(){
        var modalInstance = this.$uibModal.open({
            templateUrl: 'modules/modals/add.processing.unit.type.html',
            controller: AddProcessingUnitTypeCtrl,
            controllerAs: '$ctrl'
        });
    }
}


export default ProcessingUnitDetailsCtrl;
