import angular from 'angular';

// Create the module where our functionality can attach to
let deviceModule = angular.module('app.device', []);

// Include our UI-Router config settings
import DeviceConfig from './device.config';
deviceModule.config(DeviceConfig);

// Controllers
import DeviceCtrl from './device.controller';
deviceModule.controller('DeviceCtrl', DeviceCtrl);

import DeviceSummaryCtrl from './device-summary.controller';
deviceModule.controller('DeviceSummaryCtrl', DeviceSummaryCtrl);


export default deviceModule;
