
class DeviceSummaryCtrl {
    constructor(ProgressRail,device,$scope) {
        'ngInject';

        this.ProgressRail = ProgressRail;
        this.device = device;

        this.dataTypes = [];

        this.getSummary();

        this.getDataTypes();

        this.$scope = $scope;

        this.error = "";
    }

    getSummary() {
        this.ProgressRail
               .getSourceSummary(this.device.deviceId)
               .then(
                 (summary) => {
                     this.summary = summary;
                 });
    }

    getDataTypes() {
        this.ProgressRail
                   .getSourceFormatsBySource(this.device.deviceId)
                   .then(
                     (dataTypes) => {
                         this.dataTypes = dataTypes;
                     });
    }

    Edit(dataType) {
        
        dataType['timeInterval'] = dataType.frequency;
        dataType['timeUnit'] = dataType.unitTimeFrequency.timeUnit;

        console.log("dataType = " + JSON.stringify(dataType));
        
        this.$scope.$deviceCtrl.deviceToBeUpdated = dataType;
        this.$scope.$deviceCtrl.showDataTypeUpdate = true;
    }

    Remove(dataType) {
        this.ProgressRail
                       .deleteSourceFormatsToSource(this.device.deviceId, [dataType.sourceFormatToSourceId])
                       .then(
                         () => {
                             this.dataTypes =  _.without(this.dataTypes, _.findWhere(this.dataTypes, {
                                 sourceFormatToSourceId: dataType.sourceFormatToSourceId
                             }));
                         });
    }

    toggleStatus(dataType) {
        console.log("dataType = " + JSON.stringify(dataType));
        if(dataType.status) {
            this.disable(this.device.deviceId, dataType);
        } else {
            this.enable(this.device.deviceId, dataType);
        }
    }

    enable(sourceId, dataType) {
        console.log("dataType = " + JSON.stringify(dataType));
        let dataTypeIds = [dataType.sourceFormatToSourceId];
        this.ProgressRail
            .activateDataType(sourceId, dataTypeIds)
            .then(
                (result) => {
                    this.error = "";
                    dataType.status = true;
                },
                (error) => {
                    this.error = error.data.message;
                    dataType.status = false;
                });
    }

    disable(sourceId, dataType) {
        console.log("dataType = " + JSON.stringify(dataType));
        let dataTypeIds = [dataType.sourceFormatToSourceId];
        this.ProgressRail
            .deactivateDataType(sourceId, dataTypeIds)
            .then(
                (result) => {
                    this.error = "";
                    dataType.status = false;
                },
                (error) => {
                    this.error = error.data.message;
                    dataType.status = true;
                });
    }
}

export default DeviceSummaryCtrl;
