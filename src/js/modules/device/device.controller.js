import CreateDatatypeCtrl from '../modals/create-datatype.controller';

class DeviceCtrl {
    constructor(tags, $sessionStorage, ProgressRail, device, $uibModal, $state, timeUnits, $stateParams, $q, deviceToBeUpdated) {
        'ngInject';
        this.$storage = $sessionStorage;
        this.ProgressRail = ProgressRail;
        this.$storage.tags = tags;
        this.device = device;
        this.$uibModal = $uibModal;
        this.$state = $state;
        this.timeUnits = timeUnits;
        this.organizationId = $stateParams.organizationId;
        this.deviceId = $stateParams.deviceId;
        this.q = $q;
        this.showDataType = false;
        this.showDataTypeUpdate = false;
        this.init();
        this.getSummary();
        this.deviceToBeUpdated = deviceToBeUpdated;
        this.error = "";
    }

    init() {
        this.model = {
            selectedSourceFormatOrTags : [],
            dataTypes : [],
            selectedDataTypes : [],
            accosiatedDevice : [],
            timeInterval: null,
            timeUnit : null,
        }
    }

    getSummary() {
        this.ProgressRail
               .getSourceSummary(this.device.deviceId)
               .then(
                 (summary) => {
                     this.summary = summary;
                 });
    }

    getAssociatedDevice() {
        this.ProgressRail
                   .getSourceFormatsBySource(this.device.deviceId)
                   .then(
                     (dataTypes) => {
                         this.model.accosiatedDevice = _.pluck(dataTypes, 'sourceFormatId');
                         //console.log("this.model.accosiatedDevice = " + this.model.accosiatedDevice);
                     });
    }

    toggleStatus() {
        
        this.previousStatus = !this.summary.status;

        if(this.summary.status) {
            this.enable(this.device.deviceId);
        } else {
            this.disable(this.device.deviceId);
        }
    }

    enable(id) {
        this.ProgressRail
            .activateSource(id)
            .then(
                (result) => {
                    this.error = "";
                    this.$state.reload();
                },
                (error) => {
                    this.error = error.data.message;
                    this.summary.status = this.previousStatus;
                });
    }

    disable(id) {
        this.ProgressRail
            .deactivateSource(id)
            .then(
                (result) => {
                    this.error = "";
                    this.$state.reload();
                },
                (error) => {
                    this.error = error.data.message;
                    this.summary.status = this.previousStatus;
                });
    }

    selectDataType() {
        this.init();

        this.getAssociatedDevice();

        this.showDataType = true;
    }

    close() {
        this.showDataType = false;
    }

    closeUpdate() {
        this.showDataTypeUpdate = false;
    }

    lookUpDataTypesAndTags($viewValue) {
        var deferred = this.q.defer();
        
        this.ProgressRail
            .lookUpDataTypesAndTags(this.device.sourceCategoryId, $viewValue)
            .then((result) => {
                deferred.resolve(result);
            });
        
        return deferred.promise;
    }

    selectEntry(entry) {

        let whereCriteria = {"name": entry.name, "tagOrSourceFormat": entry.tagOrSourceFormat};

        if(_.where(this.model.selectedSourceFormatOrTags, whereCriteria).length === 0) {
            this.model.selectedSourceFormatOrTags.push(entry);
        }
        
        this.selectedTag = null;

        //console.log("selectedDataTypes - " + this.model.selectedDataTypes);

        this.refreshDataTypes();
    }

    removeEntry (entry) {
        let whereCriteria = {"name": entry.name, "tagOrSourceFormat": entry.tagOrSourceFormat};
        if(_.where(this.model.selectedSourceFormatOrTags, whereCriteria).length !== 0) {
            this.model.selectedSourceFormatOrTags = _.reject(this.model.selectedSourceFormatOrTags, function(selectedSourceFormatOrTag){
                return (selectedSourceFormatOrTag.name == entry.name && selectedSourceFormatOrTag.tagOrSourceFormat == entry.tagOrSourceFormat);
            });
        }
        this.refreshDataTypes();
    }

    refreshDataTypes() {
        this.model.dataTypes = [];

        let that = this;
        _.each(this.model.selectedSourceFormatOrTags, function(sourceFormatOrTagEntry) {
            _.each(sourceFormatOrTagEntry.sourceFormatMatches, function(sourceFormat){
                if(_.where(that.model.dataTypes, {id: sourceFormat.id}).length === 0) {
                    that.model.dataTypes.push(sourceFormat);
                }
            });
        });
    }

    clear() {
        this.init();
    }

    toggleSelection(id) {
        var idx = this.model.selectedDataTypes.indexOf(id);

        if (idx > -1) {
            this.model.selectedDataTypes.splice(idx, 1);
        }
        else {
            this.model.selectedDataTypes.push(id);
        }
    };

    addDataType() {
            this.error = "";
            if(!this.model.timeInterval || !this.model.timeUnit ||
                this.model.timeInterval.toString().indexOf('.') > -1 ||
                isNaN(this.model.timeInterval) || Number(this.model.timeInterval) <= 0 ||
                Math.floor(Number(this.model.timeInterval)) != Number(this.model.timeInterval)) {
                this.error = "Please provide valid time interval";
                return;
            }
            let deviceId = this.device.deviceId;
            let timeUnit =  this.model.timeUnit;
            let timeInterval = this.model.timeInterval;

            let data = _.map(this.model.selectedDataTypes, function(id, key) {
                return {
                    "sourceId": deviceId,
                    "sourceFormatId": id,
                    "timeUnit": timeUnit,
                    "frequency": timeInterval,
                };
            });
            this.ProgressRail
                .addSourceFormatsToSource(data)
                .then((result) => {
                    this.$state.go('app.device.main', {deviceId: this.device.deviceId, sourceCategoryId: this.device.sourceCategoryId},  {reload: true});
                });
    }

    updateDataType() {

        this.errorUpdate = "";

        if(!this.deviceToBeUpdated.timeInterval || !this.deviceToBeUpdated.timeUnit ||
            this.deviceToBeUpdated.timeInterval.toString().indexOf('.') > -1 ||
            isNaN(this.deviceToBeUpdated.timeInterval) || Number(this.deviceToBeUpdated.timeInterval) <= 0 ||
            Math.floor(Number(this.deviceToBeUpdated.timeInterval)) != Number(this.deviceToBeUpdated.timeInterval)) {
            this.errorUpdate = "Please provide valid time interval";
            return;
        }
        let sourceFormatToSourceId = this.deviceToBeUpdated.sourceFormatToSourceId;
        let timeUnit =  this.deviceToBeUpdated.timeUnit;
        let timeInterval = this.deviceToBeUpdated.timeInterval;

        let data = {
            "sourceFormatToSourceId" : sourceFormatToSourceId,
            "timeUnit": timeUnit,
            "frequency": timeInterval
        };
        
        this.ProgressRail
            .updateSourceFormatsToSource(data)
            .then((result) => {
                this.$state.go('app.device.main', {deviceId: this.device.deviceId, sourceCategoryId: this.device.sourceCategoryId},  {reload: true});
            });
    }

    createDatatype(){
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/create.datatype.html',
            controller: CreateDatatypeCtrl,
            controllerAs: '$ctrl',
            resolve: {
                sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceCategories) {
                        deferred.resolve($sessionStorage.sourceCategories);
                    }
                    else {
                        // Get source Categories
                        ProgressRail
                          .getSourceCategories()
                          .then(
                            (sourceCategories) => {
                                $sessionStorage.sourceCategories = sourceCategories;
                                deferred.resolve($sessionStorage.sourceCategories);
                            });
                    }
                    return deferred.promise;
                },
                tags : function ($q, $sessionStorage) {
                    'ngInject';
                    var deferred = $q.defer();
                    deferred.resolve($sessionStorage.tags);
                    return deferred.promise;
                },
                sourceFormatTypes: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceFormatTypes) {
                        deferred.resolve($sessionStorage.sourceFormatTypes);
                    }
                    else {
                        ProgressRail
                             .getSourceFormatTypes()
                             .then(
                               (sourceFormatTypes) => {
                                   $sessionStorage.sourceFormatTypes = sourceFormatTypes;
                                   deferred.resolve(sourceFormatTypes);
                               });
                    }
                    return deferred.promise;
                },
                presentment : function() {
                    return {'mode' : 'create'};
                }
            }
        });
    }
}

export default DeviceCtrl;
