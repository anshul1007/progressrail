function DeviceConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.device', {
        abstract: true,
        url: '/device/:organizationId/:deviceId/:sourceCategoryId',
        controller: 'DeviceCtrl',
        controllerAs: '$deviceCtrl',
        resolve: {
            device: function($stateParams) {
                return {
                    deviceId : $stateParams.deviceId,
                    sourceCategoryId : $stateParams.sourceCategoryId
                }
            },
            tags : function ($q, ProgressRail) {
                'ngInject';
                var deferred = $q.defer();
                ProgressRail
                     .getTags()
                     .then(
                       (tags) => {
                           deferred.resolve(tags);
                       });
                return deferred.promise;
            },
            timeUnits : function ($q, ProgressRail) {
                'ngInject';
                var deferred = $q.defer();
                ProgressRail
                     .getDeviceTimeUnits()
                     .then(
                       (timeUnits) => {
                           deferred.resolve(timeUnits);
                       });
                return deferred.promise;
            },
            deviceToBeUpdated : function() {
                return {};
            }
        },
        templateUrl: 'modules/device/device.html'
    })

    .state('app.device.main', {
        url:'',
        controller: 'DeviceSummaryCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/device/device-summary.html',
        title: 'Device'
    })
};

export default DeviceConfig;
