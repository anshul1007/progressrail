class HomeCtrl {
    constructor(ProgressRail, $state, Common) {
        'ngInject';

        this.ProgressRail = ProgressRail;
        this.$state = $state;
        this.summary = {};
        this.Common = Common;
        this.deadletterCount = 0;
        this.topTenIngestions = [];
        this.topTenThroughput = [];

        this.ingestionsChart =  [
            {
                key: "Ingestion Requests",
                values: [],
            }
        ];

        this.throughputChart =  [
            {
                key: "Ingestion Size",
                values: [],
            }
        ];

        this.throuputPerDay =  [
            {
                key: "Throuput Per Day",
                values: [],
            }
        ];

        this.ingestionsPerDay =  [
            {
                key: "Ingestions Per Day",
                values: [],
            }
        ];

        this.processingTime =  [
            {
                key: "Processing Time",
                values: [],
            },
            {
                key: "Ingestion Time",
                values: [],
            },
            {
                key: "Storage Time",
                values: [],
            }
        ];

        this.options = {
            chart: {
                type: 'lineChart',
                height: 300,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 65
                },
                x: function(d){ return d[0]; },
                y: function(d){ return d[1]; },
                showLegend: false,
                color: d3.scale.category10().range(),
                duration: 3600,
                useInteractiveGuideline: true,
                clipVoronoi: false,
                forceY:[0],
                xAxis: {
                    axisLabel: 'Time',
                    tickFormat: function(d) {
                        return d3.time.format('%m/%d/%y %H:%M')(new Date(d))
                    },
                    showMaxMin: false,
                    staggerLabels: true
                },

                yAxis: {
                    axisLabel: 'Size',
                    showMaxMin: false,
                    tickFormat: function(d){
                        return d; //d3.format(',.1%')(d);
                    },
                    //axisLabelDistance: 20
                }
            }
        };

        this.ingestionSizeOptions = {};
        angular.copy(this.options, this.ingestionSizeOptions);

        this.ingestionSizeOptions.chart.yAxis.tickFormat = function(d) {

            let size = d;
            let suffix = "?";

            if(size < Math.pow(2,10)) {
                suffix = " B";
            } else if(size >= Math.pow(2,10) && size < Math.pow(2,20)) {
                size = (size / Math.pow(2,10));
                suffix = " KB";
            } else if(size >= Math.pow(2,20) && size < Math.pow(2,30)) {
                size = (size / Math.pow(2,20));
                suffix = " MB";
            } else if(size >= Math.pow(2,30) && size < Math.pow(2,40)) {
                size = (size / Math.pow(2,30));
                suffix = " GB";
            } else if(size >= Math.pow(2,40)) {
                size = (size / Math.pow(2,40));
                suffix = " TB";
            }

            size = Math.round(size*100)/100;
            size = size + suffix;

            return size;
        };

        this.ingestionRequestsOptions = {};
        angular.copy(this.options, this.ingestionRequestsOptions);
        this.ingestionRequestsOptions.chart.yAxis.axisLabel = 'Count';

        this.awsOptions = {
            chart: {
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 65
                },
                x: function(d) {
                    return d[0];
                },
                y: function(d) {
                    return d[1];
                },
                //average: function(d) { return d.mean/100; },

                color: d3.scale.category10().range(),
                duration: 300,
                useInteractiveGuideline: false,
                clipVoronoi: false,
                showValues: true,
                xAxis: {
                    axisLabel: 'Time',
                    tickFormat: function(d) {
                        return d3.time.format('%m/%d/%y %H:%M')(new Date(d*1000))
                    },
                    showMaxMin: false,
                    staggerLabels: false
                },
                forceY: [0],
                yAxis: {
                    axisLabel: 'Count',
                    tickFormat: function(d) {
                        return d;
                    },
                    axisLabelDistance: 20
                }
            }
        };

        this.lineChartOptions = {
            chart: {
                type: 'cumulativeLineChart',
                height: 25,
                width: 95,
                margin : {
                    top: 5,
                    right: 5,
                    bottom: 5,
                    left: 5
                },
                x: function(d){ return d[0]; },
                y: function(d){ return d[1]/100; },
                showLegend: false,
                showControls: false,
                tooltip : {
                    enabled: false
                },
                color: ['#2CA089'],
                xAxis: {
                    tickFormat: function(d) {
                        return "";
                    },
                    showMaxMin: false,
                    staggerLabels: false
                },
                showYAxis: false,
                yAxis: {
                    //axisLabel: 'Y Axis',
                    tickFormat: function(d){
                        return d;
                    },
                }
            }
        };

        this.multiBarChart = {
            chart: {
                type: 'multiBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 45,
                    left: 45
                },
                clipEdge: true,
                duration: 500,
                stacked: true,
                xAxis: {
                    axisLabel: 'Time',
                    showMaxMin: false,
                    tickFormat: function(d){
                        return d3.time.format('%m/%d/%y %H:%M')(new Date(d*1000))
                    }
                },
                yAxis: {
                    axisLabel: 'Sum',
                    axisLabelDistance: -20,
                    showMaxMin: true,
                    tickFormat: function(d){
                        return d;
                    }
                }
            }
        };
        this.ProgressRail.getOrganizationsSummary()
            .then(
                (summary) => {
                    this.summary = summary;
                    let value = 0;
                    var throuputPerDayArray =  this.throuputPerDay[0].values;
                    _.each(this.summary.throuputPerDay, function(v,i) {
                        value += v;
                        throuputPerDayArray.push([i,value]);
                    });

                    value = 0;
                    var ingestionsPerDayArray =  this.ingestionsPerDay[0].values;
                    _.each(this.summary.ingestionsPerDay, function(v,i) {
                        value += v;
                        ingestionsPerDayArray.push([i,value]);
                    });

                }, (error) => {
                    this.error = error.data;
                });

        this.ProgressRail.getDeadletterCount()
            .then(
                (count) => {
                    this.deadletterCount = count;
                }, (error) => {
                    this.error = error.data;
                });

        //todo: replace with actual api call
        this.alertCount = "-";

        this.ProgressRail.getUnreadErrorCount()
            .then(
                (count) => {
                    this.errorCount = count;
                }, (error) => {
                    this.error = error.data;
                });

        this.dateRange = this.Common.getDates(7);

        this.getData();
    }

    getData() {

        this.ProgressRail.getTopTenIngestionsAndThroughput(this.dateRange.fromDate, this.dateRange.toDate)
            .then(
                (data) => {
                    var max = _.max(data.topTenIngestionsCharts, function(v){ return v.ingestions; }).ingestions;
                    this.topTenIngestions = _.map(data.topTenIngestionsCharts, function(v) { return {"id":  v.owner.id, "name" : v.owner.code, data: v.ingestions, value: ((v.ingestions*100.0)/max)};});

                    max = _.max(data.topTenThroughputCharts, function(v){ return v.throughput; }).throughput;
                    this.topTenThroughput = _.map(data.topTenThroughputCharts, function(v) { return {"id":  v.owner.id,"name" : v.owner.code, data: v.throughput, value: ((v.throughput*100.0)/max)};});

                }, (error) => {
                    this.error = error.data;
                });
        
        this.ProgressRail.getIngestionsAndThroughputChartsData(this.dateRange.fromDate, this.dateRange.toDate, 60*60)
            .then(
                (data) => {
                    this.ingestionsChart[0].values =  _.map(data.ingestionsChart, function(v) { return[parseInt(arguments[1])*1000, arguments[0]]});
                    this.throughputChart[0].values =  _.map(data.throughputChart, function(v) { return[parseInt(arguments[1])*1000, arguments[0]]});
                }, (error) => {
                    this.error = error.data;
                });

        this.ProgressRail.getAWSInstancesStatistics(this.dateRange.fromDate, this.dateRange.toDate, 1)
            .then(
                (data) => {
                    this.awsStatistics = _.map(_.groupBy(data, function(v) {
                        return v.role
                    }), function(m, i) {
                        return {
                            key: i,
                            values: _.map(m, function(x, y) {
                                return [x.timestamp, x.count]
                            })
                        }
                    });
                }, (error) => {
                    this.error = error.data;
                });

        this.ProgressRail.getProcessingTime(this.dateRange.fromDate, this.dateRange.toDate, 60*60)
            .then(
                (data) => {
                    this.processingTime[0].values = _.map(data, function(v,i) { return {"x": v.start_date ,"y":v.pipeline_processing_time_sum}});;
                    this.processingTime[1].values = _.map(data, function(v,i) { return {"x": v.start_date ,"y":v.ingestion_time_sum}});;
                    this.processingTime[2].values = _.map(data, function(v,i) { return {"x": v.start_date ,"y":v.storage_time_sum}});;

                }, (error) => {
                    this.error = error.data;
                });
    }
}

export default HomeCtrl;
