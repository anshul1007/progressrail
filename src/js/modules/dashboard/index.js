import angular from 'angular';

// Create the module where our functionality can attach to
let dashboardModule = angular.module('app.dashboard', []);

// Include our UI-Router config settings
import DashboardConfig from './dashboard.config';
dashboardModule.config(DashboardConfig);

// Controllers
import DashboardCostCtrl from './dashboard-cost.controller';
dashboardModule.controller('DashboardCostCtrl', DashboardCostCtrl);

import DashboardPerformanceCtrl from './dashboard-performance.controller';
dashboardModule.controller('DashboardPerformanceCtrl', DashboardPerformanceCtrl);

import DashboardUtilizationCtrl from './dashboard-utilization.controller';
dashboardModule.controller('DashboardUtilizationCtrl', DashboardUtilizationCtrl);

import DashboardSecurityCtrl from './dashboard-security.controller';
dashboardModule.controller('DashboardSecurityCtrl', DashboardSecurityCtrl);

export default dashboardModule;
