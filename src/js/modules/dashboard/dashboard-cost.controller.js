
class DashboardCostCtrl {
    constructor(ProgressRail, Common) {
        'ngInject';
      
        this.ProgressRail = ProgressRail;
        this.Common = Common;

        this.dateRange = this.Common.getDates(1);
        this.interval = 'HOUR';

        this.options = {
            chart: {
                type: 'discreteBarChart',
                height: 350,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 50,
                    left: 55
                },
                x: function(d){return d.code;},
                y: function(d){return d.sum + (1e-10);},
                showValues: true,
                valueFormat: function(d){
                    return d3.format(',.0f')(d);
                },
                duration: 500,
                xAxis: {
                    axisLabel: 'Organizations'
                },
                yAxis: {
                    axisLabel: 'Amount',
                    axisLabelDistance: -10
                }
            }
        };
        this.cost = {};
        this.organizations = [];

        this.organization = {};

        this.costSummary = [
            {
                key: "Cumulative Amount",
                values : []
            }];
        
        this.history = [
           {
               "key" : "" ,
               "bar": true,
               "values" :[]
           }];

        this.getCost();

        this.options2 = {
            chart: {
                type: 'multiBarChart',
                reduceXTicks: false,
                showControls: false,
                showLegend: false,
                height: 500,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 130,
                    left: 50
                },
                x: function(d){return d[0];},
                y: function(d){return d[1];},
                showValues: true,
                valueFormat: function(d){
                    return d3.format(',.1f')(d);
                },
                duration: 1,
                xAxis: {
                    axisLabel: 'Time',
                    tickFormat: function(d) {
                        return d3.time.format('%x %H:%M')(new Date(d));
                    },
                    rotateLabels: 90,
                    showMaxMin: false
                },
                yAxis: {
                    axisLabel: 'Amount',
                    axisLabelDistance: -10,
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                },
                tooltip: {
                    keyFormatter: function(d) {
                        return d;
                    }
                },
                zoom: {
                    enabled: false,
                    scaleExtent: [1, 10],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: true,
                    unzoomEventType: 'dblclick.zoom'
                }
            }
        };
    }

    getCost() {
        this.organization = null;
        this.history[0].values = [];
        this.ProgressRail
        .getBillingSummary(moment(this.dateRange.fromDate).format('YYYY-MM-DDTHH:mm:00'), moment(this.dateRange.toDate).format('YYYY-MM-DDTHH:mm:00'), this.interval)
        .then(
            (result) => {
                this.cost = result;
                var costSummary = [];
                _.map(result, function(cost) {
                    _.map(cost, function(item) { 
                        var hasOrganization = _.findWhere(costSummary, {id: item.organization.id});
                        if(hasOrganization) {
                            hasOrganization.sum = hasOrganization.sum + item.amount;
                        } 
                        else {
                            costSummary.push({"id" : item.organization.id, "sum" : item.amount, "name" : item.organization.name, "code" : item.organization.code})
                        }
                    })
                });
                this.costSummary[0].values = costSummary;
                this.api.refresh();
                this.organizations = _.map(costSummary, function(val) { return {"id" : val.id, "name": val.name}});
                this.organization = _.first(this.organizations);
                this.getHistroy();
            }, 
            (error) => {
                
            });
    }

    getHistroy() {
        if(this.organization){
            var result = [];
            var organizationId = this.organization.id;
            _.map(this.cost, function(outerValue, time) {
                _.map(outerValue, function(innerValue) { 
                    if(innerValue.organization.id == organizationId) { 
                        var values = [moment(time).valueOf(), innerValue.amount]; 
                        result.push(values);
                    }
                });
            });
            this.history[0].values = result;
        }
        if(this.api2) {
            this.api2.refresh();
        }
    }
}


export default DashboardCostCtrl;
