
class DashboardUtilizationCtrl {
    constructor($scope, $state, ProgressRail) {
        'ngInject';
      
        this.$scope = $scope;
        this.$state = $state;
        this.ProgressRail = ProgressRail;

        this.$scope.totalItems = 0;
        this.$scope.currentPage = 1;
        this.$scope.maxSize = 10;
        this.$scope.itemsPerPage = 25;

    }

}


export default DashboardUtilizationCtrl;
