function DashboardConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.dashboardcost', {
        url: '/dashboard-cost',
        controller: 'DashboardCostCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/dashboard/dashboard-cost.html'
    });

    $stateProvider
    .state('app.dashboardperformance', {
        url: '/dashboard-performance',
        controller: 'DashboardPerformanceCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/dashboard/dashboard-performance.html'
    });

    $stateProvider
    .state('app.dashboardutilization', {
        url: '/dashboard-utilization',
        controller: 'DashboardUtilizationCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/dashboard/dashboard-utilization.html'
    });

    $stateProvider
    .state('app.dashboardsecurity', {
        url: '/dashboard-security',
        controller: 'DashboardSecurityCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/dashboard/dashboard-security.html'
    });

};

export default DashboardConfig;
