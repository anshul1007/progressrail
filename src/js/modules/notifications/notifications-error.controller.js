
class NotificationsErrorCtrl {
    constructor($scope, $state, ProgressRail) {
        'ngInject';
      
        this.ProgressRail = ProgressRail;
        this.items = [];
        
        //this.$scope = $scope;
        //this.$scope.totalItems = 0;
        //this.$scope.currentPage = 1;
        //this.$scope.maxSize = 10;
        //this.$scope.itemsPerPage = 25;

        this.search();
    }

    pageChanged() {
        this.search();
    }

    search() {
        this.ProgressRail
                .retrieveAllError(false)
                .then(
                   (response) => {
                       //this.$scope.totalItems = response.totalElements;
                       this.items = response;
                   });
    }

    read(item) {
        this.ProgressRail
                    .markErrorAsRead(item.id)
                    .then(
                       (response) => {
                           item.read = true;
                       });
    }
}


export default NotificationsErrorCtrl;
