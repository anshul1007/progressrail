import angular from 'angular';

// Create the module where our functionality can attach to
let notificationsModule = angular.module('app.notifications', []);

// Include our UI-Router config settings
import NotificationsConfig from './notifications.config';
notificationsModule.config(NotificationsConfig);

// Controllers
import NotificationsAlertCtrl from './notifications-alert.controller';
notificationsModule.controller('NotificationsAlertCtrl', NotificationsAlertCtrl);

import NotificationsDeadletterCtrl from './notifications-deadletter.controller';
notificationsModule.controller('NotificationsDeadletterCtrl', NotificationsDeadletterCtrl);

import NotificationsErrorCtrl from './notifications-error.controller';
notificationsModule.controller('NotificationsErrorCtrl', NotificationsErrorCtrl);

export default notificationsModule;
