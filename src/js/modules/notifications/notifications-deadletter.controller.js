
class NotificationsDeadletterCtrl {
    constructor($scope, ProgressRail) {
        'ngInject';

        this.$scope = $scope;
        this.ProgressRail = ProgressRail;
        this.items = [];

        this.$scope.totalItems = 0;
        this.$scope.currentPage = 1;
        this.$scope.maxSize = 10;
        this.$scope.itemsPerPage = 25;

        this.uniqueOrganizations = [];
        this.uniqueSources = [];
        this.uniqueSourceFormats = [];

        this.deadLetterMetadataUnavailable = true;
        this.orgFilter = { "name" : "", id : ""};
        this.sourceFilter = "";
        this.sourceFormatFilter = "";
        this.sourceCategory = "";
        this.message = "";

        this.search();
    }

    pageChanged() {
        this.search();
    }

    search($item, $model, $label) {

        this.updateUIFlags();

        var filterObj = {
            "organizationId": this.orgFilter.id,
            "sourceName": this.sourceFilter,
            "sourceFormatName": this.sourceFormatFilter,
            "sourceCategory": this.sourceCategory,
            "message": this.message
        };

        let that = this;
        this.ProgressRail.getDeadletterMetaWithFilter(this.$scope.currentPage - 1, this.$scope.itemsPerPage, filterObj)
            .then((response) => {
                    this.$scope.totalItems = response.totalElements;
                    this.items = response.content;
                });

        if(this.deadLetterMetadataUnavailable) {
            this.ProgressRail
                .getDeadletterMeta()
                .then((response) => {
                    this.deadLetterMetadataUnavailable = false;
                    that.uniqueOrganizations = response.uniqueOrganizationNames;
                    that.uniqueSources = response.uniqueSourceNames;
                    that.uniqueSourceFormats = response.uniqueSourceFormatNames;
                    that.uniqueSourceCategories = response.uniqueSourceCategoryNames;
                    that.uniqueDeadLetterMessages = response.uniqueDeadLetterMessages;
                });
        }
    }

    updateUIFlags() {

        if(this.isNotBlank(this.orgFilter.id)) {
            this.orgFilterApplied = true;
        } else {
            this.orgFilterApplied = false;
        }

        if(this.isNotBlank(this.sourceFilter)) {
            this.sourceFilterApplied = true;
        } else {
            this.sourceFilterApplied = false;
        }

        if(this.isNotBlank(this.sourceFormatFilter)) {
            this.sourceFormatFilterApplied = true;
        } else {
            this.sourceFormatFilterApplied = false;
        }

        if(this.isNotBlank(this.sourceCategory)) {
            this.sourceCategoryFilterApplied = true;
        } else {
            this.sourceCategoryFilterApplied = false;
        }

        if(this.isNotBlank(this.message)) {
            this.messageFilterApplied = true;
        } else {
            this.messageFilterApplied = false;
        }
    }

    resetOrgFilter() {
        this.orgFilter = { "name" : "", id : ""};
        this.reset();
    }

    resetSourceFilter() {
        this.sourceFilter = "";
        this.reset();
    }

    resetSourceFormatFilter() {
        this.sourceFormatFilter = "";
        this.reset();
    }

    resetSourceCategoryFilter() {
        this.sourceCategory = "";
        this.reset();
    }

    resetMessageFilter() {
        this.message = "";
        this.reset();
    }

    reset() {
        this.updateUIFlags();
        this.search();
    }

    isNotBlank(val) {
        if(val && val.toString().trim() !== "") {
            return true;
        }
        return false;
    }
}


export default NotificationsDeadletterCtrl;
