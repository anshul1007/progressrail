function NotificationsConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.notifications-alert', {
        url: '/notifications-alert',
        controller: 'NotificationsAlertCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/notifications/notifications-alert.html'
    });

    $stateProvider
    .state('app.notifications-deadletter', {
        url: '/notifications-deadletter',
        controller: 'NotificationsDeadletterCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/notifications/notifications-deadletter.html'
    });

    $stateProvider
    .state('app.notifications-error', {
        url: '/notifications-error',
        controller: 'NotificationsErrorCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/notifications/notifications-error.html'
    });

};

export default NotificationsConfig;
