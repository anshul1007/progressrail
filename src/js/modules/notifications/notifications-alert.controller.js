
class NotificationsAlertCtrl {
    constructor($scope, $state, ProgressRail) {
        'ngInject';
      
        this.ProgressRail = ProgressRail;
        this.items = [];
        this.scope = 'USER';
        
        //this.$scope = $scope;
        //this.$scope.totalItems = 0;
        //this.$scope.currentPage = 1;
        //this.$scope.maxSize = 10;
        //this.$scope.itemsPerPage = 25;

        this.search();
    }

    pageChanged() {
        this.search();
    }

    search() {
        this.ProgressRail
                .retrieveAllAlert(this.scope, true, true)
                .then(
                   (response) => {
                       //this.$scope.totalItems = response.totalElements;
                       this.items = response;
                   });
    }

    silenced(item) {
        this.ProgressRail
                    .markAlertAsRead(this.scope, item.id)
                    .then(
                       (response) => {
                           item.alertRuleSilenced = true;
                           item.deactivationTime = new Date().valueOf();
                       });
    }

}


export default NotificationsAlertCtrl;
