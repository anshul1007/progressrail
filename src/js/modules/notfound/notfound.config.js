function NotFoundConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.notfound', {
            url: '/notfound',
            controllerAs: '$ctrl',
            templateUrl: 'modules/notfound/notfound.html',
            data: {
                skipLogin: true
            }
        });
};

export default NotFoundConfig;
