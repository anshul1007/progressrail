import CreateDatatypeCtrl from '../modals/create-datatype.controller';
import DeviceRegistrationCtrl from '../modals/device-registration.controller';
import ProcessingUnitDetailsCtrl from '../modals/processing-unit-details.controller';

class AppHeaderCtrl {
    constructor($uibModal, $state, $stateParams, ProgressRail, User) {
        'ngInject';
        this.$uibModal = $uibModal;
        this.ProgressRail = ProgressRail;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.User = User;
    }

    createDatatype(){
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/create.datatype.html',
            controller: CreateDatatypeCtrl,
            controllerAs: '$ctrl',
            resolve: {
                sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceCategories) {
                        deferred.resolve($sessionStorage.sourceCategories);
                    }
                    else {
                        // Get source Categories
                        ProgressRail
                          .getSourceCategories()
                          .then(
                            (sourceCategories) => {
                                $sessionStorage.sourceCategories = sourceCategories;
                                deferred.resolve($sessionStorage.sourceCategories);
                            });
                    }
                    return deferred.promise;
                },
                tags : function ($q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    ProgressRail
                         .getTags()
                         .then(
                           (tags) => {
                               deferred.resolve(tags);
                           });
                    return deferred.promise;
                },
                sourceFormatTypes: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceFormatTypes) {
                        deferred.resolve($sessionStorage.sourceFormatTypes);
                    }
                    else {
                        ProgressRail
                             .getSourceFormatTypes()
                             .then(
                               (sourceFormatTypes) => {
                                   $sessionStorage.sourceFormatTypes = sourceFormatTypes;
                                   deferred.resolve(sourceFormatTypes);
                               });
                    }
                    return deferred.promise;
                },
                presentment : function() {
                    return {'mode' : 'create'};
                }
            }
        });

        var that = this;
        modalInstance.result.then(function(dataFromModal) {
            if(dataFromModal.result == 'created' || dataFromModal.result == 'updated') {
                that.$state.transitionTo(that.$state.current, that.$stateParams, { reload: true, inherit: true, notify: true });
            }
        });
    }

    alterRules(scope) {
        this.$state.go('app.configure-alerts', {scope: scope});
        return false;
    }

    logout() {
        this.User.logout();
    }

    deviceRegistration() {
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/device.registration.html',
            controller: DeviceRegistrationCtrl,
            controllerAs: '$ctrl'
        });
    }

    addOrganization() {
        this.User.addOrganization = {
            organization: null,
            primaryContact: null
        };
        this.$state.go('app.addorganization.step1', {organizationId: null});
        return false;
    }

    addProcessingUnit() {
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/processing-unit-details.html',
            controller: ProcessingUnitDetailsCtrl,
            resolve: {
                processingUnit: function() {
                    return null;
                },
                processingUnitTypes : function ($q, $sessionStorage, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.processingUnitTypes) {
                        deferred.resolve($sessionStorage.processingUnitTypes);
                    }
                    else {
                        ProgressRail
                             .getProcessingUnitTypes()
                             .then(
                               (processingUnitTypes) => {
                                   $sessionStorage.processingUnitTypes = processingUnitTypes;
                                   deferred.resolve($sessionStorage.processingUnitTypes);
                               });
                    }
                    return deferred.promise;
                }
            },
            controllerAs: '$ctrl',
        });

    }
}

let AppHeader = {
    controller: AppHeaderCtrl,
    controllerAs : '$ctrl',
    templateUrl: 'modules/layout/header.html'
};

export default AppHeader;
