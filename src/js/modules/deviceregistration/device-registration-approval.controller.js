
class DeviceRegistrationApprovalCtrl {
    constructor($state, ProgressRail,  $stateParams, timeUnits) {
        'ngInject';
      
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.$stateParams = $stateParams;
        
        this.timeUnits = timeUnits;
        this.model = {};
        this.isAssociated = false;
        this.sourceName = "";

        this.ProgressRail
        .getDeviceRegistrationInformation(this.$stateParams.deviceRegistrationRequestId)
        .then(
          (data) => {
              this.model = data;
              if(this.model.sourceId) {
                  this.isAssociated = true;
              }
              else {
                  this.isAssociated = false;
                  this.model.tokenRotateInterval = null;
                  this.model.tokenRotateIntervalUnit = null;
                  this.model.certificateRotationInterval = null;
                  this.model.certificateRotationIntervalUnit = null;
              }
          });

    }  
    
    associate() {
        if(!this.isAssociated) {
            this.model.sourceId = null;
            this.model.sourceName = null;
            this.model.tokenRotateInterval = null;
            this.model.tokenRotateIntervalUnit = null;
            this.model.certificateRotationInterval = null;
            this.model.certificateRotationIntervalUnit = null; 
        }
    }

    sourceTypeAhead(valEntered) {
        return this.ProgressRail
            .findSourceByNameForDeviceContaining(valEntered);
    }

    sourceByName() {
        this.ProgressRail
           .findSourceByNameForDevice(this.sourceName)
           .then(
             (source) => {
                 this.applySource(source);
             });
    }

    applySource(source) {
        this.isAssociated = false;
        if(source.id && source.name) {
            this.model.sourceId = source.id;
            this.model.sourceName = source.name;
        }
        else {
            this.model.sourceId = null;
            this.model.sourceName = null;
        }
        this.model.tokenRotateInterval = null;
        this.model.tokenRotateIntervalUnit = null;
        this.model.certificateRotationInterval = null;
        this.model.certificateRotationIntervalUnit = null;
    }

    validation() {
        this.error = "";
        if(!this.model.sourceId) {
            this.error = 'Please select valid source.';
            return false;
        }
        else if(!this.isAssociated) {
            this.error = 'Please associate the source.';
            return false;
        }
        else if(!this.model.tokenRotateInterval ||
                !this.model.tokenRotateIntervalUnit ||
                !this.model.certificateRotationInterval ||
                !this.model.certificateRotationIntervalUnit) {
            this.error = 'Please enter valid interval and unit.';
            return false;
        }
        return true;
    }

    save() {
        if(this.validation()) {
            this.ProgressRail
                   .updateDeviceRegistrationInformation(this.model)
                   .then(
                     (result) => {
                         this.$state.go('app.deviceregistration');
                     }, (error) => {
                         this.error = error.data;
                     });
        }
    }

    approve() {
        if(this.validation()) {
            this.ProgressRail
                   .approveDeviceRegistrationInformation(this.model)
                   .then(
                     (result) => {
                         this.$state.go('app.deviceregistration');
                     }, (error) => {
                         this.error = error.data;
                     });
        }
    }

    changeStatus(status) {
        this.ProgressRail
               .changeDeviceRegistrationStatus(this.$stateParams.deviceRegistrationRequestId, status) 
               .then(
               (response) => {
                   this.model.mainInformation.deviceRegistrationStatus = status;
               });
    }
}


export default DeviceRegistrationApprovalCtrl;
