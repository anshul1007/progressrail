function DeviceRegistrationConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.deviceregistration', {
        url: '/deviceregistration',
        params: {
            organizationId: null,
            deviceId: null,
        },
        controller: 'DeviceRegistrationCtrl',
        controllerAs: '$ctrl',
        resolve: {
            organizations: function($q, ProgressRail) {
                var deferred = $q.defer();
                ProgressRail
                  .getOrganizationForDevice()
                  .then(
                    (organizations) => {
                        deferred.resolve(organizations);
                    });
                return deferred.promise;
            },
            devices : function($q, ProgressRail, $stateParams) {
                var deferred = $q.defer();
                if($stateParams.organizationId){
                    ProgressRail
                      .getSourceByOrganizationForDevice($stateParams.organizationId)
                      .then(
                        (devices) => {
                            deferred.resolve(devices);
                        });
                }
                else {
                    deferred.resolve([]);
                }
                return deferred.promise;
            }
        },
        templateUrl: 'modules/deviceregistration/device-registration.html'
    });

    $stateProvider
    .state('app.deviceregistrationapproval', {
        url: '/deviceregistration-approval/:deviceRegistrationRequestId',
        controller: 'DeviceRegistrationApprovalCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/deviceregistration/device-registration-approval.html',
        resolve: {
            timeUnits : function ($q, ProgressRail) {
                'ngInject';
                var deferred = $q.defer();
                ProgressRail
                     .getDeviceTimeUnits()
                     .then(
                       (timeUnits) => {
                           deferred.resolve(timeUnits);
                       });
                return deferred.promise;
            }
        }
    });
};

export default DeviceRegistrationConfig;
