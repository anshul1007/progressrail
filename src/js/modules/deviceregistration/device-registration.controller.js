
class DeviceRegistrationCtrl {
    constructor($scope, $state, ProgressRail, organizations, devices, $stateParams, AppConstants) {
        'ngInject';
      
        this.$scope = $scope;
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.organizations = organizations;
        this.devices = devices;
        this.AppConstants =  AppConstants;

        this.selection = [];

        this.searchFilter = {
            selectedOrganization : ($stateParams.organizationId === null) ? $stateParams.organizationId : _.findWhere(this.organizations, {id: Number($stateParams.organizationId)}),
            selectedDevice : ($stateParams.deviceId === null) ? $stateParams.deviceId : _.findWhere(this.devices, {id: Number($stateParams.deviceId)})
        }

        this.ProgressRail
          .getRegistrationStatus()
          .then(
            (status) => {
                this.status = status;
                this.selection = ($stateParams.organizationId === null) ? ['PENDING'] : ['APPROVED'];
                this.search();
            });

        this.$scope = angular.copy(this.AppConstants.paginationOptions);
    }

    pageChanged() {
        this.search();
    };

    search() {
        this.ProgressRail
                .searchDeviceRegistration(this.$scope.currentPage - 1, this.$scope.itemsPerPage,
                    (this.searchFilter.selectedOrganization) ? this.searchFilter.selectedOrganization.id : null, 
                    (this.searchFilter.selectedDevice) ? this.searchFilter.selectedDevice.id : null,
                    this.selection) 
                .then(
                (response) => {
                    this.$scope.totalItems = response.totalHits;
                    this.items = response.result;
                });
    }

    toggleSelection(status) {
        var idx = this.selection.indexOf(status);

        if (idx > -1) {
            this.selection.splice(idx, 1);
        }
        else {
            this.selection.push(status);
        }
    };

    changeStatus(item, status) {
        this.ProgressRail
               .changeDeviceRegistrationStatus(item.mainInformation.deviceRegistrationRequestId, status) 
               .then(
               (response) => {
                   item.mainInformation.deviceRegistrationStatus = status;
               });
    };

    edit(deviceRegistrationRequestId) {
        this.$state.go('app.deviceregistrationapproval', {deviceRegistrationRequestId : deviceRegistrationRequestId});
    }

    selectOrganization($item, $model, $label) {
        this.searchFilter.selectedOrganization = $item;
        this.ProgressRail
          .getSourceByOrganizationForDevice(this.searchFilter.selectedOrganization.id)
          .then(
            (devices) => {
                this.devices = devices;
            });
    }

    removeOrganization() {
        this.searchFilter.selectedOrganization = null;
        this.selectedOrganization = null;
        this.searchFilter.selectedDevice = null;
        this.selectedDevice = null;
    }

    selectDevice($item, $model, $label) {
        this.searchFilter.selectedDevice = $item;
    }

    removeDevice() {
        this.searchFilter.selectedDevice = null;
        this.selectedDevice = null;
    }

    downloadCredentials($event, clientId, registrationId, force) {
    
        $event.preventDefault();
        
        if(!force) {
            this.ProgressRail
                .getRegistrationCredentials(clientId, registrationId)
                .then(
                    (result) => {
                        var blob = new Blob([JSON.stringify(result)]);
                        saveAs(blob, registrationId+".json");
                    });
        } else {
            this.ProgressRail
                .getRenewedRegistrationCredentials(clientId, registrationId)
                .then(
                    (result) => {
                        var blob = new Blob([JSON.stringify(result)]);
                        saveAs(blob, registrationId+".json");
                    });
        }
    }
}


export default DeviceRegistrationCtrl;
