import angular from 'angular';

// Create the module where our functionality can attach to
let deviceregistrationModule = angular.module('app.deviceregistration', []);

// Include our UI-Router config settings
import DeviceRegistrationConfig from './device-registration.config';
deviceregistrationModule.config(DeviceRegistrationConfig);

// Controllers
import DeviceRegistrationCtrl from './device-registration.controller';
deviceregistrationModule.controller('DeviceRegistrationCtrl', DeviceRegistrationCtrl);

import DeviceRegistrationApprovalCtrl from './device-registration-approval.controller';
deviceregistrationModule.controller('DeviceRegistrationApprovalCtrl', DeviceRegistrationApprovalCtrl);

export default deviceregistrationModule;
