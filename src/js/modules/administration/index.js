import angular from 'angular';

// Create the module where our functionality can attach to
let administrationModule = angular.module('app.administration', []);

// Include our UI-Router config settings
import AdministrationConfig from './administration.config';
administrationModule.config(AdministrationConfig);

// Controllers
import AdministrationRoleCtrl from './administration-role.controller';
administrationModule.controller('AdministrationRoleCtrl', AdministrationRoleCtrl);

import AdministrationUserCtrl from './administration-user.controller';
administrationModule.controller('AdministrationUserCtrl', AdministrationUserCtrl);

import ConfigureAlertRule from './configure-alert-rule.controller';
administrationModule.controller('ConfigureAlertRule', ConfigureAlertRule);

import ConfigureAlertsCtrl from './configure-alerts.controller';
administrationModule.controller('ConfigureAlertsCtrl', ConfigureAlertsCtrl);

export default administrationModule;
