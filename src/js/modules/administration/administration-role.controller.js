
class AdministrationRoleCtrl {
    constructor(ProgressRail, permissions) {
        'ngInject';
        this.ProgressRail = ProgressRail;
        this.getRoles();
        this.permissions = permissions;

        this.roles = [];
        this.selectedRole = null;
        this.users = null;
    }

    getRoles() {
        this.ProgressRail
              .getAllRoles()
              .then(
                (roles) => {
                    this.roles = roles;
                });
    }

    selectRole(role) {
        this.users = null;
        this.selectedRole = role;
        this.assignedRoles = angular.copy(role.permissions);
    }

    selectPermission(permission) {
        var idx = this.selectedRole.permissions.indexOf(permission);

        if (idx > -1) {
            this.selectedRole.permissions.splice(idx, 1);
        }
        else {
            this.selectedRole.permissions.push(permission);
        }
    }

    updateRole() {
        this.error = "";
        if (!this.selectedRole.roleName) {
            this.error = "Please provide role name";
            return;
        }
        var data = { 
            "id": this.selectedRole.id, 
            "roleName": this.selectedRole.roleName,
            "description" : this.selectedRole.description,
            "permissions": this.selectedRole.permissions
        };
        this.ProgressRail
             .updateRole(data)
             .then(
               () => {
                   
               }, (error) => {
                   this.error = error.data;
               });

        var newPermissions = _.reject(this.selectedRole.permissions, function(val) { return this.assignedRoles.indexOf(val) > -1 }, this);
        if(newPermissions.length > 0) {
            this.ProgressRail.addPermissionsToRole(this.selectedRole.id, newPermissions)
                  .then(
                    (data) => {
                    }, (error) => {
                        this.error = error.data;
                    });
        }
        var deletePermissions = _.reject(this.assignedRoles, function(val){ return this.selectedRole.permissions.indexOf(val) > -1 }, this);
        if(deletePermissions.length > 0) {
            this.ProgressRail.deletePermissionsFromRole(this.selectedRole.id, deletePermissions)
                  .then(
                    (data) => {
                    }, (error) => {
                        this.error = error.data;
                    });
        }
    }

    addNewRole() {
        this.users = null;
        this.error = "";
        this.selectedRole = { "roleName": "", "permissions": [], "description": ""};
    }

    saveNewRole() {
        this.error = "";
        if (!this.selectedRole.roleName) {
            this.error = "Please provide role name";
            return;
        }
        var data = this.selectedRole;
        this.ProgressRail
             .addRole(data)
             .then(
               (role) => {
                   this.selectedRole.id = role.id;
                   this.ProgressRail
                   .addPermissionsToRole(this.selectedRole.id, data.permissions)
                   .then(
                        (id) => {

                        }, (error) => {
                            this.error = error.data;
                        });
                   this.selectedRole.createdDttm = role.createdDttm;
                   this.roles.push(angular.copy(this.selectedRole));
                   this.selectedRole = null;
               }, (error) => {
                   this.error = error.data;
               });

       
    }

    deleteRole() {
        this.error = "";
        if(this.selectedRole && this.selectedRole.id) {
            this.ProgressRail
                .deleteRole(this.selectedRole.id)
                .then(
                  (id) => {
                      var idx = _.find(this.roles, { "id" : this.selectedRole.id });
                      if (idx) {
                          this.roles = _.without(this.roles, idx);
                          this.selectedRole = null;
                      }
                  }, (error) => {
                      this.error = error.data;
                  });
        }
    }

    viewUsers(id) {
        this.ProgressRail
              .getUsersByRole(id)
              .then(
                (users) => {
                    this.users = users;
                });
    }
}


export default AdministrationRoleCtrl;
