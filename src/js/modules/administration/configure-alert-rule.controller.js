
class ConfigureAlertRule {
    constructor(ProgressRail, matrics, User, $state, $stateParams) {
        'ngInject';
        this.matrics = matrics;
        this.ProgressRail = ProgressRail;
        this.User = User;
        this.$state = $state;
        this.scope = ($stateParams.scope !== null) ? $stateParams.scope : 'USER';

        this.model = {
            id: ($stateParams.alertRuleId !== null) ? $stateParams.alertRuleId : 0,
            alertName: '',
            metric: '',
            comparisonType: 'GREATEREQUAL',
            numericValue: '',
            durationAmount: '',
            durationUnit: ''
        };
        if($stateParams.alertRuleId) {
            this.ProgressRail
                .retrieveSingleMetricAlertRules(this.scope, $stateParams.alertRuleId)
                    .then(
                    (result) => {
                        this.model.alertName = result.alertName;
                        this.model.metric = result.metric;
                        this.model.comparisonType = result.comparisonType;
                        this.model.numericValue = result.numericValue;
                        this.model.durationAmount = result.durationAmount;
                        this.model.durationUnit = result.durationUnit;
                    });
        }
    }

    addAlertRule() {
        var data = this.model;
        this.ProgressRail
            .metricAlertRuleAddUpdate(data, this.scope, true)
                .then(
                (rule) => {
                    this.$state.go('app.configure-alerts', {scope: this.scope});
                });
    }
}


export default ConfigureAlertRule;
