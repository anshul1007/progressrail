import ChangePasswordCtrl from '../modals/change-password.controller';
import ManageRolesCtrl from '../modals/manage-roles.controller';
import ManageOrganizationsCtrl from '../modals/manage-organizations.controller';


class AdministrationUserCtrl {
    constructor(ProgressRail, roles, organizations, $uibModal, $q) {
        'ngInject';
        this.ProgressRail = ProgressRail;
        this.$uibModal = $uibModal;
        this.$q = $q;
        this.selectedUserId = null;
        this.selectedTab = 0;
        this.userValidate = false;
        this.getUsers();
        this.roles = roles;
        this.organizations = organizations;
        this.searchString = '';
        this.searchActive = true;
        this.globalOrganization = {};
    }

    searchfilter(user) {
        return (!this.searchString || user.firstName.toLowerCase().indexOf(this.searchString.toLowerCase()) > -1 
            ||  user.lastName.toLowerCase().indexOf(this.searchString.toLowerCase()) > -1) 
            && (this.searchActive === null || user.active === this.searchActive);
    }

    init() {
        this.model = {
            "username": "", 
            "password": "", 
            "firstName": "", 
            "lastName": "", 
            "email": "", 
            "address": "", 
            "country": "", 
            "state": "", 
            "city": "", 
            "phoneNumber": ""
        };
        this.userValidate = false;
        this.selectedTab = 0;
    }

    addNewUser() {
        this.init();
        this.selectedUserId = 0;
    }

    selectUser(user) {
        this.init();
        this.selectedUserId = user.id;
        this.model = user;
    }

    getUsers() {
        this.ProgressRail.getUsers(0, 1000)
                 .then(
                   (data) => {
                       this.users = data.result;
                   }, (error) => {
                       this.error = error.data;
                   });
    }

    save(form) {
        this.userValidate = true;
        this.error = "";
        if(form.$valid && this.model.password === this.model.confirmPassword) {
            this.ProgressRail.addUser(this.model)
             .then(
               (user) => {
                   this.model.id = user.id;
                   this.model.createdDttm = user.createdDttm;
                   this.users.push(angular.copy(this.model));
                   this.selectedUserId = null;
               }, (error) => {
                   this.error = error.data;
               });
        }
    }

    update(form) {
        this.userValidate = true;
        this.error = "";
        if(form.$valid) {
            this.ProgressRail.updateUser(this.model)
             .then(
               (id) => {
                   this.selectedUserId = null;
               }, (error) => {
                   this.error = error.data;
               });
        }
    }
    
    changePassword() {
        let selectedUserId = this.selectedUserId;
        let modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/change.password.html',
            controller: ChangePasswordCtrl,
            resolve: {
                userId : function() {
                    return selectedUserId;
                }
            },
            controllerAs: '$ctrl'
        });
    }

    toggleUserStatus(status) {
        this.error = '';
        if(status) {
            this.ProgressRail.makeInactiveActive(this.selectedUserId)
                .then(
                (data) => {
                    this.model.active = false;
                }, (error) => {
                    this.error = error.data;
                });
        }
        else {
            this.ProgressRail.makeUserActive(this.selectedUserId)
                .then(
                (data) => {
                    this.model.active = true;
                }, (error) => {
                    this.error = error.data;
                });
        }
    }

    getSelectedOrganizations() {
        let deferred = this.$q.defer();
        this.ProgressRail.getOrganizationsAndRoleByUser(this.selectedUserId)
              .then(
                (data) => {
                    //this.assignedOrganizations = data;
                    this.selectedOrganizations = data;
                    this.globalOrganization = _.find(data, {'global':true}) || 
                                                                                    { 
                                                                                        "global": true,
                                                                                        "organizationId": null,
                                                                                        "organizationName": null,
                                                                                        "organizationCode": null,
                                                                                        "roles": [] 
                                                                                    };
                    deferred.resolve();
                }, (error) => {
                    this.error = error.data;
                    deferred.reject();
                });
        return deferred.promise;
    }

    addOrganization() {
        let $uibModal = this.$uibModal;
        let organizations = this.organizations;
        let userId = this.selectedUserId;
        let roles = this.roles;
        let selectedOrganizations = _.map(this.selectedOrganizations, function(v,i) { return {"id" : v.organizationId}});
        let that = this;

        let showOrganizations = function(global) {
            let modalInstance = $uibModal.open({
                templateUrl: 'modules/modals/manage.organizations.html',
                controller: ManageOrganizationsCtrl,
                resolve: {
                    organizations : function() {
                        return organizations;
                    },
                    roles : function() {
                        return roles;
                    },
                    selectedOrganizations : function() {
                        return selectedOrganizations || [];
                    },
                    userId : function() {
                        return userId;
                    }
                },
                controllerAs: '$ctrl'
            });

            modalInstance.result.then(angular.noop, function (e) {
                that.getSelectedOrganizations();
            });
        };
        showOrganizations();
    }

    manageRoles(organization) {
        let $uibModal = this.$uibModal;
        let userId = this.selectedUserId;
        let organizationId = organization.organizationId;
        let organizationCode = organization.organizationCode;
        let roles = this.roles;
        let assignedRoles= organization.roles;
        let that = this;

        let showRoles = function(global) {
            let modalInstance = $uibModal.open({
                templateUrl: 'modules/modals/manage.roles.html',
                controller: ManageRolesCtrl,
                resolve: {
                    roles : function() {
                        return roles;
                    },
                    selectedRoles : function() {
                        return assignedRoles;
                    },
                    organizationCode : function() {
                        return organizationCode;
                    },
                    userId : function() {
                        return userId;
                    },
                    organizationId: function() {
                        return organizationId;
                    },
                    global : function() {
                        return global;
                    }
                },
                controllerAs: '$ctrl'
            });

            modalInstance.result.then(angular.noop, function (e) {
                that.getSelectedOrganizations();
            });
        };

        showRoles(organization.global);
    }

    removeOrganization(organization) {
        this.ProgressRail.removeRolesFromOrganizationsAndUser(this.selectedUserId, organization.organizationId, _.pluck(organization.roles, "id"))
                  .then(
                    (data) => {
                        this.getSelectedOrganizations();
                    }, (error) => {
                        this.error = error.data;
                    });
    }
}


export default AdministrationUserCtrl;
