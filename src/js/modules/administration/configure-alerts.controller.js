
class ConfigureAlertsCtrl {
    constructor(ProgressRail, User, $state, $stateParams) {
        'ngInject';
      
        this.ProgressRail = ProgressRail;
        this.items = [];
        this.User = User;
        this.$state = $state;
        this.scope = ($stateParams.scope !== null) ? $stateParams.scope : 'USER';
        this.search();
    }

    search() {
        this.ProgressRail
                .retrieveAllMetricAlertRules(this.scope)
                .then(
                   (response) => {
                       this.items = response;
                   });
    }

    silenced(item) {
        this.error = "";
        this.ProgressRail
             .setMetricAlertRuleSilenced(this.scope, item.id)
             .then(
               (result) => {
                   item.silenced = true;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

    unsilenced(item) {
        this.error = "";
        this.ProgressRail
             .setMetricAlertRuleUnsilenced(this.scope, item.id)
             .then(
               (result) => {
                   item.silenced = false;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

    sync(item) {
        this.error = "";
        this.ProgressRail
             .syncOfMetricAlertRule(this.scope, item.id)
             .then(
               (result) => {
                   item.synced = true;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

    addRule() {
        this.$state.go('app.configure-alert-rule', {scope: this.scope});
    }

    edit(item) {
        this.$state.go('app.configure-alert-rule', {alertRuleId: item.id, scope: this.scope});
    }

    delete(item) {
        this.error = "";
        this.ProgressRail
             .syncOfMetricAlertRule(this.scope, item.id)
             .then(
               (result) => {
                   this.ProgressRail
                     .deleteMetricAlertRule(this.scope, item.id)
                     .then(
                           (result) => {
                               var idx = this.items.indexOf(item);

                               if (idx > -1) {
                                   this.items.splice(idx, 1);
                               }
                           }, 
                           (error) => {
                               this.error = error.data;
                           });
               }, 
               (error) => {
                   this.error = error.data;
               });
    }
}


export default ConfigureAlertsCtrl;
