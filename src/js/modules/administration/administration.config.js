function AdministrationConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.administration-role', {
        url: '/administration-role',
        controller: 'AdministrationRoleCtrl',
        controllerAs: '$ctrl',
        resolve: {
            permissions: function($q, ProgressRail) {
                var deferred = $q.defer();
                ProgressRail
                    .getAllPermissionsList()
                    .then(
                    (permissions) => {
                        deferred.resolve(permissions);
                    });
                return deferred.promise;
            }
        },
        templateUrl: 'modules/administration/administration-role.html'
    });

    $stateProvider
    .state('app.administration-user', {
        url: '/administration-user',
        controller: 'AdministrationUserCtrl',
        controllerAs: '$ctrl',
        resolve: {
            roles: function($q, ProgressRail) {
                var deferred = $q.defer();
                ProgressRail
                     .getAllRoles()
                     .then(
                       (roles) => {
                           deferred.resolve(roles);
                       });
                return deferred.promise;
            },
            organizations: function($q, ProgressRail) {
                var deferred = $q.defer();
                ProgressRail
                  .getOrganizations()
                  .then(
                    (organizations) => {
                        deferred.resolve(organizations);
                    });
                return deferred.promise;
            }
        },
        templateUrl: 'modules/administration/administration-user.html'
    });

    $stateProvider
    .state('app.configure-alert-rule', {
        url: '/configure-alert-rule',
        params: {
            alertRuleId: null,
            scope: null
        },
        controller: 'ConfigureAlertRule',
        controllerAs: '$ctrl',
        resolve: {
            matrics: function($q, ProgressRail) {
                var deferred = $q.defer();
                ProgressRail
                  .getAvailableMatrics()
                  .then(
                    (matrics) => {
                        deferred.resolve(matrics);
                    });
                return deferred.promise;
            }
        },
        templateUrl: 'modules/administration/configure-alert-rule.html'
    });

    $stateProvider
   .state('app.configure-alerts', {
       url: '/configure-alerts',
       params: {
           scope: null
       },
       controller: 'ConfigureAlertsCtrl',
       controllerAs: '$ctrl',
       templateUrl: 'modules/administration/configure-alerts.html'
   });

};

export default AdministrationConfig;
