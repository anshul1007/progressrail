import CreateDatatypeCtrl from '../modals/create-datatype.controller';

class DataTypeListCtrl {
    constructor(ProgressRail, $sessionStorage, $state, $stateParams, sourceCategories, $uibModal) {
        'ngInject';

        this.ProgressRail = ProgressRail;
        this.$storage = $sessionStorage;
        this.$state = $state;
        this.$uibModal = $uibModal;
        this.$stateParams = $stateParams;

        this.ProgressRail.getDataTypes()
              .then(
                (data) => {

                    // update the response array with source category name
                    let extendedData = _.map(data, function(entry, index){
                        let catId = entry.sourceCategoryId;
                        let catObj = _.find(sourceCategories, function(sourceCategory) {
                            if(sourceCategory.id == catId) {
                                return true;
                            }
                        });
                        entry['sourceCategoryName'] = catObj.name;
                        return entry;
                    });

                    // Lets pluck the tag ids from response
                    let respTags = _.pluck(extendedData, 'tags');

                    // Lookup for tagNames from session storage
                    let tagObjsArr = _.map(respTags, function(tagIdArr) {
                        let tagObjArr = _.map(tagIdArr, function(tagId){
                            let tagObj = _.find($sessionStorage.tags, function(sessionStorageTag){
                                if(sessionStorageTag.id == tagId) {
                                    return true;
                                }
                            });
                            return tagObj;
                        });
                        return tagObjArr;
                    });

                    // update the response array with the tag names
                    extendedData = _.map(extendedData, function(entry, index){
                        if(extendedData.length >0 && extendedData.length ===  tagObjsArr.length) {
                            let tagObjArr = tagObjsArr[index];
                            let tagNames = _.pluck(tagObjArr, 'name');
                            entry['tagNames'] = tagNames.join(', ');
                            entry['tagObjs'] = tagObjArr;
                        } else {
                            entry['tagNames'] = '';
                            entry['tagObjs'] = [];
                        }
                        return entry;
                    });

                    // show response on screen
                    this.dataTypes = extendedData;
                    
                }, (error) => {
                    this.error = error.data;
                });
    }

    edit(entry) {

        let selectedDataTypeObj = {
            "id": entry.id,
            "name": entry.name,
            "version": entry.version,
            "description": entry.description,
            "selectedTags": entry.tagObjs,
            "validate": false,
            "sourceFormatType": entry.sourceFormatType,
            "sourceCategoryName" : entry.sourceCategoryName
        };
        
        this.updateDateType(selectedDataTypeObj);
    }

    delete(entry) {
        var that = this;
        // delete data type
        this.ProgressRail
            .deleteDataType(entry.id)
            .then(() => {that.reloadPage()});
    }

    updateDateType(dataType){

        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/create.datatype.html',
            controller: CreateDatatypeCtrl,
            controllerAs: '$ctrl',
            resolve: {
                sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceCategories) {
                        deferred.resolve($sessionStorage.sourceCategories);
                    }
                    else {
                        // Get source Categories
                        ProgressRail
                            .getSourceCategories()
                            .then(
                                (sourceCategories) => {
                                    $sessionStorage.sourceCategories = sourceCategories;
                                    deferred.resolve($sessionStorage.sourceCategories);
                                });
                    }
                    return deferred.promise;
                },
                tags : function ($q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    ProgressRail
                        .getTags()
                        .then(
                            (tags) => {
                                deferred.resolve(tags);
                            });
                    return deferred.promise;
                },
                sourceFormatTypes: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceFormatTypes) {
                        deferred.resolve($sessionStorage.sourceFormatTypes);
                    }
                    else {
                        ProgressRail
                            .getSourceFormatTypes()
                            .then(
                                (sourceFormatTypes) => {
                                    $sessionStorage.sourceFormatTypes = sourceFormatTypes;
                                    deferred.resolve(sourceFormatTypes);
                                });
                    }
                    return deferred.promise;
                },
                presentment : function() {
                    let stateObj = {
                        'mode' : 'update',
                        'selectedDataType' : dataType
                    };
                    return stateObj;
                }
            }
        });

        var that = this;
        modalInstance.result.then(function(dataFromModal) {
            if(dataFromModal.result == 'created' || dataFromModal.result == 'updated') {
                that.reloadPage();
            }
        });

    }

    reloadPage() {
        this.$state.transitionTo(this.$state.current, this.$stateParams, { reload: true, inherit: true, notify: true });
    }
}

export default DataTypeListCtrl;
