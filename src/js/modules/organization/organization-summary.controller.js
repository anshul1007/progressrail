class OrganizationSummaryCtrl {
    constructor($rootScope, ProgressRail, Common, AppConstants, Mapper, $uibModal) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.ProgressRail = ProgressRail;
        this.Common = Common;
        this.AppConstants = AppConstants;
        this.Mapper = Mapper;
        this.$uibModal = $uibModal;
        this.organizationId = this.$rootScope.organizationId;

        this.devices = [];

        this.dateRange = this.Common.getDates(1); // default one day

        // Get summary
        this.ProgressRail
            .getSummaryData(this.organizationId)
            .then((summary) => {
                this.summary = summary;
            });

        this.ProgressRail
            .getSourceByOrganizationForOrganizationViewSummary(this.organizationId)
            .then((devices) => {
                this.devices = devices;

                this.category1Devices = this.getDevicesByCategory(devices,1); // Locomotives
                this.category2Devices = this.getDevicesByCategory(devices,2); // Applications
                this.category3Devices = this.getDevicesByCategory(devices,3); // Wayside

            });

        this.options = this.AppConstants.chartOptions.throughputData;

        this.getThroughputData = (data) => {
            this.ProgressRail
                .getThroughputData(this.organizationId, this.dateRange)
                .then((throughputData) => {
                    this.data =  this.Mapper.throughputData(throughputData);
                });
        }

        this.getThroughputData();
    }

   getDevicesByCategory(devices, categoryId) {
       return _.filter(devices, function(device){
           if(device['sourceCategoryId'] == categoryId) {
               return true;
           }
           return false;
       });
   }

    showData(days) {
        if (days >= -1) {
            this.dateRange = this.Common.getDates(days);
            this.getThroughputData();
        }
        else if (days === -2) {
            this.dateRange.days = -2;
        }
    }
        
    go(fromDate, toDate) {
        this.dateRange.fromDate = fromDate;
        this.dateRange.toDate = toDate;
        this.getThroughputData();
    }
}

export default OrganizationSummaryCtrl;
