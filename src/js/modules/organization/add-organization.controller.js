class AddOrganizationCtrl {
    constructor($rootScope, $state) {
        'ngInject';
        this.$state = $state;
        this.$rootScope = $rootScope;
    }

    cancel() {
        this.$state.go('app.organization.main', { organizationId : this.$rootScope.organizationId });
    }
}

export default AddOrganizationCtrl;
