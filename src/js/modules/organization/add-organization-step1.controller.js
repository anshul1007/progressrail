class AddOrganizationStep1Ctrl {
    constructor($state, $scope, $stateParams, ProgressRail, User) {
        'ngInject';
        this.$scope = $scope;
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.User = User;

        if($stateParams.organizationId !== null && 
            !(this.User.addOrganization && this.User.addOrganization.organization && this.User.addOrganization.organization.id === $stateParams.organizationId)) {
            // get organization details
            this.ProgressRail
              .getOrganizationDetails($stateParams.organizationId)
              .then(
                (organization) => {
                    var address = organization.addresses[0] || {
                        id: null,
                        address1 : '',
                        country: '',
                        zipCode: '',
                        state: '',
                        city: '',
                        phoneNumber: ''
                    };

                    var contacts = organization.contacts[0] || {
                        id: null,
                        firstName: '',
                        lastName: '',
                        email: '',
                        phoneNumber: ''
                    };

                    this.User.addOrganization = {
                        organization : {
                            id : organization.id,
                            addressId: address.id,
                            code : organization.code,
                            description: organization.description,
                            companyName: organization.name,
                            address: address.address1,
                            country: address.country,
                            postalCode: address.zipCode,
                            state: address.state,
                            city: address.city,
                            phoneNumber: address.phoneNumber,
                            isValid : true
                        },
                        primaryContact : {
                            id : contacts.id,
                            firstName: contacts.firstName,
                            lastName: contacts.lastName,
                            email: contacts.email,
                            phoneNumber: contacts.phoneNumber,
                            isValid: false
                        },
                        geoRestriction : {
                            countries: organization.countries,
                            oldCountries: angular.copy(organization.countries),
                            list : (organization.countries && organization.countries.length > 0 && organization.countries[0].blacklist) ? "Blacklist" : "Whitelist",
                            geo: (organization.countries && organization.countries.length > 0) ? "Yes" : "No",
                            isValid : true
                        },
                        awsKmsKeyArn : organization.awsKmsKeyArn,
                        validation : {
                            organization : true,
                            primaryContact : true
                        },
                        step1Validate : true
                    };
                }, (error) => {
                    this.error = error.data;
                });
        }
        else if (!(this.User.addOrganization && this.User.addOrganization.organization && this.User.addOrganization.primaryContact)) {
            this.User.addOrganization = {
                organization : {
                    code : '',
                    companyName: '',
                    description : '',
                    address: '',
                    country: '',
                    postalCode: '',
                    state: '',
                    city: '',
                    phoneNumber: '',
                    isValid : false
                },
                primaryContact : {
                    firstName: '',
                    lastName: '',
                    email:'',
                    phoneNumber: '',
                    isValid: false
                },
                geoRestriction : {
                    countries: [],
                    list : "Whitelist",
                    geo: "No",
                    isValid : false
                },
                awsKmsKeyArn : '',
                validation : {
                    organization : false,
                    primaryContact : false
                },
                step1Validate : false
            };
        }

        if(this.User.addOrganization && this.User.addOrganization.organization) {
            var organizationWatcher = this.$scope.$watch(() => this.User.addOrganization.organization, () => {
                if(this.User.addOrganization && this.User.addOrganization.validation && this.User.addOrganization.validation.organization) {
                    this.User.addOrganization.validation.organization = false;
                }
            }, true);
        }
        if(this.User.addOrganization && this.User.addOrganization.primaryContact) {
            var primaryContactWatcher = this.$scope.$watch(() => this.User.addOrganization.primaryContact, () => {
                if(this.User.addOrganization && this.User.addOrganization.validation && this.User.addOrganization.validation.primaryContact) {
                    this.User.addOrganization.validation.primaryContact  = false;
                }
            }, true);
        }
        this.$scope.$on("$destroy", function() {
            if(organizationWatcher) {
                organizationWatcher();
            }
            if(primaryContactWatcher) {
                primaryContactWatcher();
            }
        });
    }

    next(form) {
        this.User.addOrganization.step1Validate = true;
        if(form.$valid) {
            if( this.$state.current.name  === 'app.addorganization.step1') {
                this.User.addOrganization.validation.organization = true;
                this.User.addOrganization.validation.primaryContact = true;
                this.$state.go('^.step2');
            }
        }
    }

}

export default AddOrganizationStep1Ctrl;
