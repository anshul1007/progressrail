import AddDeviceCtrl from '../modals/add-device.controller';

class OrganizationCtrl {
    constructor(ProgressRail, $rootScope, $state, User, $uibModal) {
        'ngInject';
        this.ProgressRail = ProgressRail;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.User = User;
        this.$uibModal = $uibModal;
        this.organizationId = this.$rootScope.organizationId;
        this.previousStatus = false;
        this.error = "";
        // Get summary
        this.ProgressRail
          .getGeneralOrganizationData( this.organizationId)
          .then(
            (summary) => {
                this.summary = summary;
                this.$rootScope.organizationCode = this.summary.organizationCode;
            });
    }

    editOrganization() {
        this.User.addOrganization = {
            organization: null,
            primaryContact: null
        };
        this.$state.go('app.addorganization.step1', {organizationId: this.organizationId});
    }

    addDevice() {
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/add.device.html',
            controller: AddDeviceCtrl,
            controllerAs: '$ctrl',
            resolve: {
                sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceCategories) {
                        deferred.resolve($sessionStorage.sourceCategories);
                    }
                    else {
                        // Get source Categories
                        ProgressRail
                          .getSourceCategories()
                          .then(
                            (sourceCategories) => {
                                $sessionStorage.sourceCategories = sourceCategories;
                                deferred.resolve($sessionStorage.sourceCategories);
                            });
                    }
                    return deferred.promise;
                }
            }
        });
    }

    toggleStatus() {

        this.previousStatus = !this.summary.active;
        
        if(this.summary.active) {
            this.enable(this.organizationId);
        } else {
            this.disable(this.organizationId);
        }
    }

    enable(id) {
        this.ProgressRail
            .activateOrganizqation(id)
            .then(
                (result) => {
                },
                (error) => {
                    this.error = error.data;
                    this.summary.active = this.previousStatus;
                });
    }

    disable(id) {
        this.ProgressRail
            .deactivateOrganizqation(id)
            .then(
                (result) => {
                },
                (error) => {
                    this.error = error.data;
                    this.summary.active = this.previousStatus;
                });
    }
}


export default OrganizationCtrl;
