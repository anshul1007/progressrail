import AddDeviceCtrl from '../modals/add-device.controller';

class OrganizationDeviceListCtrl {
    constructor($rootScope, ProgressRail, $state, Common, AppConstants, Mapper, $uibModal) {
        'ngInject';
        
        this.$rootScope = $rootScope;
        this.ProgressRail = ProgressRail;
        this.Common = Common;
        this.AppConstants = AppConstants;
        this.Mapper = Mapper;
        this.$uibModal = $uibModal;

        this.organizationId = this.$rootScope.organizationId

        this.devices = [];

        this.dateRange = this.Common.getDates(1); // default one day

        this.ProgressRail
            .getSourceByOrganizationForOrganizationViewSummary(this.organizationId)
            .then((devices) => {
                this.devices = this.getDevicesByCategory(devices,$state.params.deviceCategoryId); // Locomotives
                /*this.category2Devices = this.getDevicesByCategory(devices,2); // Applications
                this.category3Devices = this.getDevicesByCategory(devices,3); // Wayside*/
            });

        this.options = this.AppConstants.chartOptions.throughputData;

        this.getThroughputData = (data) => {
            this.ProgressRail
                .getThroughputData(this.organizationId, this.dateRange)
                .then((throughputData) => {
                    this.data =  this.Mapper.throughputData(throughputData);
                });
        };

        this.getThroughputData();
    }

    getDevicesByCategory(devices, categoryId) {
        return _.filter(devices, function(device){
            if(device['sourceCategoryId'] == categoryId) {
                return true;
            }
            return false;
        });
    }

    showData(days) {
        if (days >= -1) {
            this.dateRange = this.Common.getDates(days);
            this.getThroughputData();
        }
        else if (days === -2) {
            this.dateRange.days = -2;
        }
    }

    go(fromDate, toDate) {
        this.dateRange.fromDate = fromDate;
        this.dateRange.toDate = toDate;
        this.getThroughputData();
    }

    addDevice() {
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/modals/add.device.html',
            controller: AddDeviceCtrl,
            controllerAs: '$ctrl',
            resolve: {
                sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                    'ngInject';
                    var deferred = $q.defer();
                    if($sessionStorage.sourceCategories) {
                        deferred.resolve($sessionStorage.sourceCategories);
                    }
                    else {
                        // Get source Categories
                        ProgressRail
                            .getSourceCategories()
                            .then(
                                (sourceCategories) => {
                                    $sessionStorage.sourceCategories = sourceCategories;
                                    deferred.resolve($sessionStorage.sourceCategories);
                                });
                    }
                    return deferred.promise;
                }
            }
        });
    }
}

export default OrganizationDeviceListCtrl;
