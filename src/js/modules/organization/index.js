import angular from 'angular';

// Create the module where our functionality can attach to
let organizationModule = angular.module('app.organization', []);

// Include our UI-Router config settings
import OrganizationConfig from './organization.config';
organizationModule.config(OrganizationConfig);

// Controllers
import OrganizationCtrl from './organization.controller';
organizationModule.controller('OrganizationCtrl', OrganizationCtrl);

import OrganizationSummaryCtrl from './organization-summary.controller';
organizationModule.controller('OrganizationSummaryCtrl', OrganizationSummaryCtrl);

// Controllers
import AddOrganizationCtrl from './add-organization.controller';
organizationModule.controller('AddOrganizationCtrl', AddOrganizationCtrl);

import AddOrganizationStep1Ctrl from './add-organization-step1.controller';
organizationModule.controller('AddOrganizationStep1Ctrl', AddOrganizationStep1Ctrl);

import AddOrganizationStep2Ctrl from './add-organization-step2.controller';
organizationModule.controller('AddOrganizationStep2Ctrl', AddOrganizationStep2Ctrl);

import AddOrganizationStep3Ctrl from './add-organization-step3.controller';
organizationModule.controller('AddOrganizationStep3Ctrl', AddOrganizationStep3Ctrl);

import OrganizationListCtrl from './organization-list.controller';
organizationModule.controller('OrganizationListCtrl', OrganizationListCtrl);

import OrganizationDeviceListCtrl from './organization-device-list.controller';
organizationModule.controller('OrganizationDeviceListCtrl', OrganizationDeviceListCtrl);

import DataTypeListCtrl from './datatype-list.controller';
organizationModule.controller('DataTypeListCtrl', DataTypeListCtrl);

export default organizationModule;
