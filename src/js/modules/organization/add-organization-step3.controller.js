class AddOrganizationStep3Ctrl {
    constructor(User, $state, ProgressRail, $q) {
        'ngInject';
        this.User = User;
        this.$state = $state;
        this.ProgressRail = ProgressRail;
        this.$q = $q;
    }


    save() {
        if(this.User.addOrganization.validation.organization  && this.User.addOrganization.validation.primaryContact) {
            if(!this.User.addOrganization.organization.id) {
                var data = {
                    "code": this.User.addOrganization.organization.code,
                    "name": this.User.addOrganization.organization.companyName,
                    "description" : this.User.addOrganization.organization.description,
                    "awsKmsKeyArn" : this.User.addOrganization.awsKmsKeyArn,
                    "status": true,
                    "addresses": [
                      {
                          "country": this.User.addOrganization.organization.country,
                          "city": this.User.addOrganization.organization.city,
                          "state": this.User.addOrganization.organization.state,
                          "address1": this.User.addOrganization.organization.address,
                          "zipCode": this.User.addOrganization.organization.postalCode,
                          "phoneNumber": this.User.addOrganization.organization.phoneNumber
                      }
                    ],
                    "contacts": [
                      {
                          "firstName": this.User.addOrganization.primaryContact.firstName,
                          "lastName":  this.User.addOrganization.primaryContact.lastName,
                          "initial": true,
                          "email": this.User.addOrganization.primaryContact.email,
                          "phoneNumber": this.User.addOrganization.primaryContact.phoneNumber,
                      }
                    ],
                    countries : (this.User.addOrganization.geoRestriction.geo === 'Yes') 
                        ? _.map(this.User.addOrganization.geoRestriction.countries, function(v) {return {"id": v.id, "blacklist": v.blacklist, "geoRestriction": v.geoRestriction }}) 
                        : [],
                    //list: this.User.addOrganization.geoRestriction.list,
                    //geo: this.User.addOrganization.geoRestriction.geo
                };

                this.error = null;
                // add organization
                this.ProgressRail
                  .addOrganization(data)
                  .then(
                    (id) => {
                        if(id) {
                            this.User.addOrganization = null;
                            this.$state.go('app.organization.main.devices', { organizationId : id, deviceCategoryId : 1});
                        }
                    }, (error) => {
                        this.error = error.data;
                    });
            }
            else {
                let promises = [];
                let data = { 
                    "id": this.User.addOrganization.organization.id,
                    "code": this.User.addOrganization.organization.code,  
                    "name": this.User.addOrganization.organization.companyName, 
                    "description": this.User.addOrganization.organization.description,
                    "awsKmsKeyArn" : this.User.addOrganization.awsKmsKeyArn
                };

                // update organization
                let organizationsPromise = this.ProgressRail
                  .updateOrganization(data)
                  .then(
                    (id) => {
                        if(id) {
                            //this.User.addOrganization = null;
                            //this.$state.go('app.organization.main', { organizationId : id});
                        }
                    }, (error) => {
                        this.error = error.data;
                    });
                promises.push(organizationsPromise);

                let address = {
                    "id": this.User.addOrganization.organization.addressId, 
                    "country": this.User.addOrganization.organization.country,
                    "city": this.User.addOrganization.organization.city, 
                    "state": this.User.addOrganization.organization.state,
                    "address1": this.User.addOrganization.organization.address, 
                    "address2": "", 
                    "phoneNumber": this.User.addOrganization.organization.phoneNumber,
                    "zipCode": this.User.addOrganization.organization.postalCode, 
                    "organizationId": this.User.addOrganization.organization.id 
                };
                
                let addressPromise = (this.User.addOrganization.organization.addressId) 
                    ? this.ProgressRail // update organization address
                      .updateOrganizationAddress(address)
                      .then(
                        (id) => {
                            if(id) {
                                //this.User.addOrganization = null;
                                //this.$state.go('app.organization.main', { organizationId : id});
                            }
                        }, (error) => {
                            this.error = error.data;
                        }) 
                    : this.ProgressRail // add organization address
                      .addOrganizationAddress(address)
                      .then(
                        (id) => {
                            if(id) {
                                //this.User.addOrganization = null;
                                //this.$state.go('app.organization.main', { organizationId : id});
                            }
                        }, (error) => {
                            this.error = error.data;
                        });
                promises.push(addressPromise);

                let contact = { 
                    "id": this.User.addOrganization.primaryContact.id, 
                    "firstName": this.User.addOrganization.primaryContact.firstName, 
                    "lastName": this.User.addOrganization.primaryContact.lastName, 
                    "email": this.User.addOrganization.primaryContact.email, 
                    "phoneNumber": this.User.addOrganization.primaryContact.phoneNumber, 
                    "initial": true, 
                    "organizationId": this.User.addOrganization.organization.id  
                };
               
                let contactPromise = (this.User.addOrganization.primaryContact.id) 
                    ? this.ProgressRail  // update organization contact
                          .updateOrganizationContact(contact)
                          .then(
                            (id) => {
                                if(id) {
                                    //this.User.addOrganization = null;
                                    //this.$state.go('app.organization.main', { organizationId : id});
                                }
                            }, (error) => {
                                this.error = error.data;
                            })
                    :  this.ProgressRail
                          .addOrganizationContact(contact)  // add organization contact
                          .then(
                            (id) => {
                                if(id) {
                                    //this.User.addOrganization = null;
                                    //this.$state.go('app.organization.main', { organizationId : id});
                                }
                            }, (error) => {
                                this.error = error.data;
                            });
                promises.push(contactPromise);

                var deleteCountries = this.User.addOrganization.geoRestriction.oldCountries;
                if(deleteCountries && deleteCountries.length > 0 ) {
                    let data = {
                        "id": this.User.addOrganization.organization.id,
                        "countries": _.map(deleteCountries, function(v) { return { "id": v.id, "blacklist": v.blacklist, "geoRestriction": v.geoRestriction }})
                    }
                    let deleteCountriesPromise = this.ProgressRail.deleteCountriesToOrganization(data)
                         .then(
                           (data) => {
                           }, (error) => {
                               this.error = error.data;
                           });
                    promises.push(deleteCountriesPromise);
                }

                var newCountries = this.User.addOrganization.geoRestriction.countries;
                if(newCountries && newCountries.length > 0) {
                    let data = {
                        "id": this.User.addOrganization.organization.id,
                        "countries": _.map(newCountries, function(v) { return { "id": v.id, "blacklist": v.blacklist, "geoRestriction": v.geoRestriction }})
                    }
                    let newCountriesPromise = this.ProgressRail.addCountriesToOrganization(data)
                          .then(
                            (data) => {
                            }, (error) => {
                                this.error = error.data;
                            });
                    promises.push(newCountriesPromise);
                }
                

                var that  = this;
                this.$q.all(promises).then(function(result) {
                    that.User.addOrganization = null;
                    that.$state.go('app.organization-list');
                });
            }
        }
        else {
            this.$state.go('^.step1');
        }
    }
}

export default AddOrganizationStep3Ctrl;
