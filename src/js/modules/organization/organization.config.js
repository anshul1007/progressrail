function OrganizationConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.organization', {
        abstract: true,
        url: '/organization/:organizationId',
        controller: 'OrganizationCtrl',
        controllerAs: '$ctrl',
        resolve: {
            organization: function($state, $stateParams, $rootScope) {
                $rootScope.organizationId = $stateParams.organizationId;
            }
        },
        templateUrl: 'modules/organization/organization.html'
    })
    .state('app.organization.main', {
        url:'',
        controller: 'OrganizationSummaryCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/organization/organization-summary.html',
        title: 'Organization',
        resolve: {
            deviceCategoryIdParent: function($state, $stateParams, $rootScope) {
                $rootScope.organizationId = $stateParams.organizationId;
            }
        }
    })
    .state('app.organization.main.devices', {
        url:'/:deviceCategoryId',
        controller: 'OrganizationDeviceListCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/organization/organization-device-list.html',
        title: 'OrganizationDevices',
        params : {
            organizationId : '',
            deviceCategoryId : ''
        }
    })
    .state('app.organization-list', {
        url: '/organization-list',
        controller: 'OrganizationListCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/organization/organization-list.html'
    })
    .state('app.datatype-list', {
        url: '/datatype-list',
        controller: 'DataTypeListCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/organization/datatype-list.html',
        resolve: {
            sourceCategories: function ($sessionStorage, $q, ProgressRail) {
                'ngInject';
                var deferred = $q.defer();
                if ($sessionStorage.sourceCategories) {
                    deferred.resolve($sessionStorage.sourceCategories);
                }
                else {
                    // Get source Categories
                    ProgressRail
                        .getSourceCategories()
                        .then(
                            (sourceCategories) => {
                                $sessionStorage.sourceCategories = sourceCategories;
                                deferred.resolve($sessionStorage.sourceCategories);
                            });
                }
                return deferred.promise;
            }
        }
    })
    .state('app.addorganization', {
        abstract: true,
        params: {
            organizationId: null,
        },
        url: '/addorganization',
        controller: 'AddOrganizationCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/organization/add-organization.html'
    })
        .state('app.addorganization.step1', {
            url: '/step1',
            controller: 'AddOrganizationStep1Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/organization/add-organization-step1.html',
            title: 'Add Organization: Step 1'
        })
        .state('app.addorganization.step2', {
            url: '/step2',
            controller: 'AddOrganizationStep2Ctrl',
            resolve: {
                countries: function($sessionStorage, $q, ProgressRail) {
                    var deferred = $q.defer();
                    if($sessionStorage.countries) {
                        deferred.resolve($sessionStorage.countries);
                    }
                    else {
                        // Get countries
                        ProgressRail
                          .getCountries()
                          .then(
                            (countries) => {
                                $sessionStorage.countries = countries;
                                deferred.resolve($sessionStorage.countries);
                            });
                    }
                    return deferred.promise;
                }
            },
            controllerAs: '$ctrl',
            templateUrl: 'modules/organization/add-organization-step2.html',
            title: 'Add Organization: Step 2'
        })
        .state('app.addorganization.step3', {
            url: '/step3',
            controller: 'AddOrganizationStep3Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/organization/add-organization-step3.html',
            title: 'Add Organization: Step 3'
        });

};

export default OrganizationConfig;
