class AddOrganizationStep2Ctrl {
    constructor(User, $state, countries) {
        'ngInject';
        this.User = User;
        this.$state = $state;
        this.countries = countries;
        
        this.selectedCountry = null;

        if (!(this.User.addOrganization && this.User.addOrganization.geoRestriction)) {
            this.User.addOrganization = {
                organization : {
                    code : '',
                    companyName: '',
                    description : '',
                    address: '',
                    country: '',
                    postalCode: '',
                    state: '',
                    city: '',
                    phoneNumber: '',
                    isValid : false
                },
                primaryContact : {
                    firstName: '',
                    lastName: '',
                    email:'',
                    phoneNumber: '',
                    isValid: false
                },
                geoRestriction : {
                    countries: [],
                    list : "Whitelist",
                    geo: "No",
                    isValid : false
                },
                awsKmsKeyArn : '',
                validation : {
                    organization : false,
                    primaryContact : false
                },
                step1Validate : false
            };
        }
    }

    clearCountry() {
        this.User.addOrganization.geoRestriction.countries = [];
    }

    selectCountry (item) {
        if(_.where(this.User.addOrganization.geoRestriction.countries, {id: item.id}).length === 0) {
            item.blacklist = (this.User.addOrganization.geoRestriction.list !== 'Whitelist');
            item.geoRestriction = (this.User.addOrganization.geoRestriction.geo !== 'No');
            this.User.addOrganization.geoRestriction.countries.push(item);
        }
        this.selectedCountry = null;
    }

    removeCountry (id) {
        if(_.where(this.User.addOrganization.geoRestriction.countries, {id: id}).length !== 0) {
            this.User.addOrganization.geoRestriction.countries = _.reject(this.User.addOrganization.geoRestriction.countries, function(objArr){ return objArr.id == id; });
        }
    }

    next (form) {
        if(form.$valid){
            if(this.$state.current.name  === 'app.addorganization.step2') {
                this.User.addOrganization.geoRestriction.isValid = true;
                this.$state.go('^.step3');
            }
        }
    }
}

export default AddOrganizationStep2Ctrl;
