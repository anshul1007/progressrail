
class OrganizationListCtrl {
    constructor(ProgressRail, User, $state) {
        'ngInject';

        this.ProgressRail = ProgressRail;
        this.User = User;
        this.$state = $state;
        
        this.ProgressRail.getCurrentUserOrganization()
              .then(
                (data) => {
                    this.organizations = data;
                }, (error) => {
                    this.error = error.data;
                });
    }

    edit(item) {
        this.User.addOrganization = {
            organization: null,
            primaryContact: null
        };
        this.$state.go('app.addorganization.step1', {organizationId: item.id});
    }

    enable(item) {
        this.error = "";
        this.ProgressRail
             .activateOrganizqation(item.id)
             .then(
               (result) => {
                   item.status = true;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

    disable(item) {
        this.error = "";
        this.ProgressRail
             .deactivateOrganizqation(item.id)
             .then(
               (result) => {
                   item.status = false;
               }, 
               (error) => {
                   this.error = error.data;
               });
    }

}

export default OrganizationListCtrl;
