﻿import angular from 'angular';

let filtersModule = angular.module('app.filters', []);

import Tel from './tel.filter';
filtersModule.filter('tel', Tel);

import Time from './time.filter';
filtersModule.filter('time', Time);

export default filtersModule;
