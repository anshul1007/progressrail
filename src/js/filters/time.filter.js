﻿function Time() {
    'ngInject';

    return function (utcSeconds) {
        //console.log(tel);
        if (!utcSeconds) { return ''; }

        var d = new Date(0);
        return new Date(d.setUTCSeconds(utcSeconds));
    };
}

export default Time;
