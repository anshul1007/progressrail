var fs = require("fs");

var appRouter = function(app) {

	// --------------- get ------------------------
	app.get("/api/organization/summary/generalOrganizationData", function(req, res) {
		fs.readFile("api/getGeneralOrganizationData.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization/summary/summaryOrganizationData", function(req, res) {
		fs.readFile("api/getSummaryOrganizationData.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization/countries", function(req, res) {
		fs.readFile("api/getCountries.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization/summary/throughputData", function(req, res) {
		fs.readFile("api/getThroughputData.json", 'utf8', function (err, data) {
			if(req.query.id != '2'){
				res.end(data);
			}
			else {
				res.end("[]");
			}
		});
	});

	app.get("/api/source/categories", function(req, res) {
		fs.readFile("api/getSourceCategories.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/tags", function(req, res) {
		fs.readFile("api/getTags.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization", function(req, res) {
		fs.readFile("api/getOrganizations.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/device/organizations", function(req, res) {
		fs.readFile("api/getOrganizations.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/view/organizations", function(req, res) {
		fs.readFile("api/getOrganizations.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/manage/organizations", function(req, res) {
		fs.readFile("api/getOrganizations.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/formats", function(req, res) {
		fs.readFile("api/getDataTypes.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/sources", function(req, res) {
		fs.readFile("api/getDevice.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/processingUnit/byType", function(req, res) {
		fs.readFile("api/getProcessingUnitsbyType.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	/*app.get("/api/source/byOrganizationId", function(req, res) {
		// return res.status(403).send('Something broke!@@');
		fs.readFile("api/getDevicesbyOrganization.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});*/
	
	app.get("/api/device/source/byOrganizationId", function(req, res) {
		// return res.status(403).send('Something broke!@@');
		fs.readFile("api/getDevicesbyOrganization.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/view/source/byOrganizationId", function(req, res) {
		// return res.status(403).send('Something broke!@@');
		fs.readFile("api/getDevicesbyOrganization.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/formats/bySourceCategoryId", function(req, res) {
		fs.readFile("api/getDataTypesbySourceCategory.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/manage/source/byOrganizationId", function(req, res) {
		// return res.status(403).send('Something broke!@@');
		fs.readFile("api/getDevicesbyOrganization.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization/summary/source/byOrganizationId", function(req, res) {
		fs.readFile("api/getDataTypesbySourceCategory.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/test", function(req, res) {
		fs.readFile("api/testPipeline.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/searchPaged", function(req, res) {
		// return res.status(403).send('Something broke!@@');
		fs.readFile("api/searchPipelinePaged.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/byId", function(req, res) {
		fs.readFile("api/getPipelineById.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/lastTestResult", function(req, res) {
		fs.readFile("api/getPipelineLastTestResult.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/device/statuses", function(req, res) {
		res.send(['PENDING', 'APPROVED', 'REJECTED', 'EXPIRED']);
	});
	
	app.get("/api/device/searchPaged", function(req, res) {
		fs.readFile("api/searchDeviceRegistration.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/device", function(req, res) {
		fs.readFile("api/getDeviceRegistrationInformation.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	/*app.get("/api/source/byName", function(req, res) {
		fs.readFile("api/sourceByName.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});*/
	
	app.get("/api/device/source/byName", function(req, res) {
		fs.readFile("api/sourceByName.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/formats/byCategoryAndTags", function(req, res) {
		fs.readFile("api/getSourceFormatsByCategoryandTags.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/formats/bySource", function(req, res) {
		fs.readFile("api/getSourceFormatsBySource.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/allTestResults", function(req, res) {
		fs.readFile("api/getAllTestResultForPipeline.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/testOutput", function(req, res) {
		fs.readFile("api/getAllTestResultForPipeline.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/device/timeUnits", function(req, res) {
		res.send(['DAY', 'HOUR', 'MINUTE', 'SECOND', 'MILLISECOND']);
	});
	
	app.get("/api/role/permissions", function(req, res) {
		res.send([ "ADMIN", "CREATE_ORGANIZATION", "CREATE_SOURCE", "ASSOCIATE_SOURCE_TO_FORMAT", "CREATE_SOURCE_FORMAT", "CREATE_PIPELINE", "UPDATE_PIPELINE", "TEST_PIPELINE", "VIEW_PIPELINE", "VIEW_DEVICE_REGISTRATION", "UPDATE_DEVICE_REGISTRATION", "VIEW_DEVICE_STATE", "USER_MANAGEMENT" ]);
	});
	
	app.get("/api/role/all", function(req, res) {
		fs.readFile("api/getRoles.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/role/users", function(req, res) {
		fs.readFile("api/getUsersByRole.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/pipeline/approvalStatuses", function(req, res) {
		res.send([ "INCOMPLETE", "READY_FOR_TEST", "TEST_PASSED", "READY_FOR_APPROVAL", "APPROVED" ]);
	});
	
	app.get("/api/user/searchPaged", function(req, res) {
		fs.readFile("api/getPaginatedSearchUser.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/user/organization/roles", function(req, res) {
		fs.readFile("api/getOrganizationsAndRoleByUser.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/user/organizationsByUser", function(req, res) {
		fs.readFile("api/getOrganizationsByUser.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/source/format/types", function(req, res) {
		res.send([ "DATA", "DOCUMENT", "VIDEO1" ]);
	});
	
	app.get("/api/user/current", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		res.send({"firstName": "a", "lastName": "b", "permissions": [ "VIEW_PIPELINE", "ADMIN" ], "username": "admin"});
	});
	
	app.get("/api/user/current/organizations", function(req, res) {
		fs.readFile("api/getCurrentUserOrganization.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/source/summary", function(req, res) {
		fs.readFile("api/getSourceSummary.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/organization/summary/usageStatistics", function(req, res) {
		fs.readFile("api/getBillingSummary.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/processingUnit/all", function(req, res) {
		fs.readFile("api/getAllProcessingUnits.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/processingUnit/types", function(req, res) {
		fs.readFile("api/getProcessingUnitTypes.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/processingUnit/searchPaged", function(req, res) {
		fs.readFile("api/searchProcessingUnits.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});		
	});
	
	app.get("/api/alerts/rules/supportedMetrics", function(req, res) {
		fs.readFile("api/getAvailableMatrics.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/alerts/rules/SYSTEM/metric", function(req, res) {
		fs.readFile("api/getAllMatrics.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/alerts/rules/USER/metric", function(req, res) {
		fs.readFile("api/getAllMatrics.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/alerts/rules/SYSTEM/metric/1", function(req, res) {
		fs.readFile("api/getSingleMatric.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/alerts/rules/USER/metric/1", function(req, res) {
		fs.readFile("api/getSingleMatric.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/alerts/SYSTEM/metric", function(req, res) {
		fs.readFile("api/retrieveAllAlert.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/alerts/USER/metric", function(req, res) {
		fs.readFile("api/retrieveAllAlert.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.get("/api/organization/summary/all/summaryData", function(req, res) {
		fs.readFile("api/getOrganizationsSummary.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/device/credentials", function(req, res) {
		fs.readFile("api/getRegistrationCredentials.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/deadletter/count", function(req, res) {
		return res.send("12");
	});
	
	app.get("/api/deadletter", function(req, res) {
		fs.readFile("api/getDeadletter.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/organization/fullData", function(req, res) {
		fs.readFile("api/getOrganizationDetails.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/organization/summary/all/topTenIngestions", function(req, res) {
		fs.readFile("api/getTopTenIngestionsAndThroughput.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/organization/summary/all/ingestionCharts", function(req, res) {
		fs.readFile("api/getIngestionsAndThroughputChartsData.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/home/awsinstances", function(req, res) {
		fs.readFile("api/getAWSInstancesStatistics.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/organization/summary/timeStatistics", function(req, res) {
		fs.readFile("api/getProcessingTime.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/api/error", function(req, res) {
		fs.readFile("api/retrieveAllError.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});	
	});
	
	app.get("/account", function(req, res) {
		var accountMock = {
			"username": "nraboy",
			"password": "1234",
			"twitter": "@nraboy"
		}
		if(!req.query.username) {
			return res.send({"status": "error", "message": "missing username"});
		} else if(req.query.username != accountMock.username) {
			return res.send({"status": "error", "message": "wrong username"});
		} else {
			return res.send(accountMock);
		}
	});
	
	// --------------- post ------------------------
	app.post("/api/security/login", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send({"firstName": "a", "lastName": "b", "permissions": [ {"authority": "ADMIN"} ], "username": "admin"});
	});
	
	app.post("/api/organization", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("PRS2");
	});
	
	app.post("/api/source", function(req, res) {
		return res.status(400).send('Something broke!@@');
		//return res.send("123");
	});
	
	app.post("/api/source/format", function(req, res) {
		return res.send("345");
	});
	
	app.post("/api/source/tag", function(req, res) {
		return res.send("546");
	});
	
	app.post("/api/pipeline", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.post("/api/processingUnit", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.post("/api/processingUnit/type", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.post("/account", function(req, res) {
		if(!req.body.username || !req.body.password || !req.body.twitter) {
			return res.send({"status": "error", "message": "missing a parameter"});
		} else {
			return res.send(req.body);
		}
	});
	
	app.post("/api/source/formats/toSource", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.post("/api/user", function(req, res) {
		return res.send({"id":2, "username":"danylo", "firstName":"Danyl1o", "lastName":"AHL1","email":"asdasd@g.com","address":"Bara str", "country":"Kambodga", "state":"Abra","city":"Bamboo","phoneNumber":"309582","active":true, "createdDttm":"2016-11-09 14:49:45.0","createdBy":"admin", "lastUpdatedDttm":"2016-11-09 17:46:52.0", "lastUpdatedBy":"admin"});
	});
	
	app.post("/api/role", function(req, res) {
		return res.send({ "id":2, "roleName": "First Role", "description": "role", "createdDttm": "07-12-2016 08:20:29", "createdBy": "admin" });
	});
	
	app.post("/api/user/roles", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/user/toOrganizations", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/user/roles/toOrganization", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/organization/address", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/organization/contact", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/role/permissions", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/alerts/rules/SYSTEM/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/alerts/rules/USER/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.post("/api/organization/countries", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/pipeline", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST'});	
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST', 'validationErrors': [ 'Processing unit JSON Field Remover configuration must have \'field\'!', 'Pipeline must contain one distribution unit!' ] });	
		// return res.send({"id":77,"approvalStatus":"INCOMPLETE","validationErrors":["Configuration parameter(s) [passwords] for SFTP Distribution is not allowed!","Configuration parameter(s) [password] for SFTP Distribution are obligatory!"]});		
	});
	
	app.put("/api/pipeline/changeStatus", function(req, res) {
		return res.send("345");
		//return res.status(400).send('Something broke!@@');
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST'});	
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST', 'validationErrors': [ 'Processing unit JSON Field Remover configuration must have \'field\'!', 'Pipeline must contain one distribution unit!' ] });	
		// return res.send({"id":77,"approvalStatus":"INCOMPLETE","validationErrors":["Configuration parameter(s) [passwords] for SFTP Distribution is not allowed!","Configuration parameter(s) [password] for SFTP Distribution are obligatory!"]});		
	});
	
	app.put("/api/device/changeStatus", function(req, res) {
		return res.send();
		//return res.status(400).send('Something broke!@@');
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST'});	
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST', 'validationErrors': [ 'Processing unit JSON Field Remover configuration must have \'field\'!', 'Pipeline must contain one distribution unit!' ] });	
		// return res.send({"id":77,"approvalStatus":"INCOMPLETE","validationErrors":["Configuration parameter(s) [passwords] for SFTP Distribution is not allowed!","Configuration parameter(s) [password] for SFTP Distribution are obligatory!"]});		
	});
	
	app.put("/api/device", function(req, res) {
		//console.log(req.body);
		return res.send();
		//return res.status(400).send('Something broke!@@');
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST'});	
		//return res.send({ 'id': 73, 'approvalStatus': 'READY_FOR_TEST', 'validationErrors': [ 'Processing unit JSON Field Remover configuration must have \'field\'!', 'Pipeline must contain one distribution unit!' ] });	
		// return res.send({"id":77,"approvalStatus":"INCOMPLETE","validationErrors":["Configuration parameter(s) [passwords] for SFTP Distribution is not allowed!","Configuration parameter(s) [password] for SFTP Distribution are obligatory!"]});		
	});
	
	app.put("/api/device/approve", function(req, res) {
		//console.log(req.body);
		return res.send();
	});

	app.put("/api/role", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/user", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/user/active", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/processingUnit", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/processingUnit/activate", function(req, res) {
		return res.status(400).send('Something broke!@@');
		//return res.send("12");
	});
	
	app.put("/api/device/register", function(req, res) {
		fs.readFile("api/deviceRegister.json", 'utf8', function (err, data) {
			// console.log( data );
			res.end( data );
		});
	});
	
	app.put("/api/user/password", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/user/role/global", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/organization", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/organization/address", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/organization/contact", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/organization/activate", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("12");
	});
	
	app.put("/api/alerts/rules/SYSTEM/metric", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/alerts/rules/USER/metric", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/alerts/rules/SYSTEM/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/alerts/rules/USER/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.put("/api/error/markAsRead", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/source/formats/fromSource", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.delete("/api/security/login", function(req, res) {
		//return res.status(400).send('Something broke!@@');
		return res.send("73");
	});
	
	app.delete("/api/role", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/user/roles", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/user/toOrganizations", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/user/roles/toOrganization", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/role/permissions", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/user/active", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/processingUnit/activate", function(req, res) {
		return res.status(400).send('Something broke!@@');
		//return res.send("12");
	});
	
	app.delete("/api/organization/activate", function(req, res) {
		return res.status(400).send('Something broke!@@');
		//return res.send("12");
	});
	
	app.delete("/api/alerts/rules/SYSTEM/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/rules/USER/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/rules/SYSTEM/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/rules/USER/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/SYSTEM/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/USER/metric/1/silence", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/SYSTEM/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/alerts/USER/metric/1", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/user/role/global", function(req, res) {
		return res.send("12");
	});
	
	app.delete("/api/organization/countries", function(req, res) {
		return res.send("12");
	});
}
 
module.exports = appRouter;